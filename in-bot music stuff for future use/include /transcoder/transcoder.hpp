#pragma once

#include <dpp/cluster.h>
#include <dpp/dpp.h>

#include <string>
extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavcodec/codec.h>
#include <libavformat/avformat.h>
#include <libavutil/audio_fifo.h>
#include <libswresample/swresample.h>
}

namespace commands::downloader::transcoder
{


class OpusTranscoder
{
   public:
    enum class Result
    {
        fail,
        success
    };

    OpusTranscoder() = delete;
    OpusTranscoder(const std::string& inFileName,
                   const std::string& outFileName, dpp::cluster* bot);

    Result transcode();

   private:
    int openInput();
    int openOutput();
    int initResampler();
    int writeOutputHeader();

    class InputAVFormatContext
    {
        AVFormatContext* ctx;

       public:
        InputAVFormatContext();
        ~InputAVFormatContext();
        int openInput(const std::string& fileName);
        AVFormatContext* get();
        AVFormatContext* operator->();
    };

    class OutputAVFormatContext
    {
        AVFormatContext* ctx;

       public:
        OutputAVFormatContext();
        ~OutputAVFormatContext();
        int openOutput(const std::string& fileName);
        AVFormatContext* get();
        AVFormatContext* operator->();
    };

    class SmartAVCodecContext
    {
        AVCodecContext* ctx;

       public:
        SmartAVCodecContext();
        ~SmartAVCodecContext();
        void init(const AVCodec* codec);
        AVCodecContext* get();
        AVCodecContext* operator->();
    };

    class SmartResampleContext
    {
        SwrContext* ctx;

       public:
        SmartResampleContext();
        ~SmartResampleContext();
        int alloc(SmartAVCodecContext& inCodecCtx,
                  SmartAVCodecContext& outCodecCtx);
        SwrContext* get();
        SwrContext* operator->();
    };

    class SmartFifo
    {
        AVAudioFifo* fifo;

       public:
        SmartFifo();
        ~SmartFifo();
        int alloc(SmartAVCodecContext& outCodecCtx);
        AVAudioFifo* get();
        AVAudioFifo* operator->();
    };

    const std::string inFileName;
    const std::string outFileName;
    dpp::cluster* bot;

    InputAVFormatContext inFormatCtx{};
    OutputAVFormatContext outFormatCtx{};

    SmartAVCodecContext inCodecCtx{};
    SmartAVCodecContext outCodecCtx{};
    SmartResampleContext resampleCtx{};
    SmartFifo fifo{};
};
}  // namespace commands::downloader::transcoder
