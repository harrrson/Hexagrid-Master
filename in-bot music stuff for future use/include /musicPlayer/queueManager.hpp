#pragma once
#include <dpp/dispatcher.h>
#include <dpp/snowflake.h>
#include <dpp/utility.h>

namespace musicCommand::queueManager
{
struct QueueElement
{
    enum class Type
    {
        file = 0
    };
    Type sourceType;
    std::string title{};
    std::string url{};
    time_t downloadExpiration{};
    std::string filePath{};
};

void createQueueForGuild(const dpp::slashcommand_t&);
void addElementToQueue(dpp::snowflake, QueueElement);
bool isQueueEmpty(dpp::snowflake);
QueueElement& getElement(dpp::snowflake);
void popElement(dpp::snowflake, bool);
}  // namespace musicCommand::queueManager
