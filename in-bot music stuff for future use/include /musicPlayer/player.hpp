#pragma once

#include <dpp/cluster.h>
#include <dpp/discordclient.h>
#include <dpp/snowflake.h>

#include <condition_variable>

namespace musicCommand::player
{
struct PlayerController
{
    bool alive{true};
    dpp::snowflake guildId;
    dpp::cluster *bot;
    dpp::discord_client *client;
    std::condition_variable cvWait;
    std::mutex mutCv;

    PlayerController(dpp::snowflake guildId, dpp::cluster *bot,
                     dpp::discord_client *client)
        : guildId(guildId), bot(bot), client(client)
    {
    }
};

void playerThread(PlayerController *);
}  // namespace musicCommand::player
