#pragma once
#include <dpp/cluster.h>
#include <dpp/dispatcher.h>
#include <dpp/snowflake.h>

namespace musicCommand::playerManager
{
bool isPlayerAssigned(dpp::snowflake);
void assignPlayer(const dpp::slashcommand_t&);
void releasePlayer(const dpp::slashcommand_t&);
void notifyPlayerNewQueueElement(dpp::snowflake);
}  // namespace musicCommand::playerManager
