#include "commands/downloader/transcoder/transcoder.hpp"

#include <dpp/misc-enum.h>

#include <memory>

extern "C"
{
#include <libavutil/channel_layout.h>
#include <libavutil/log.h>
}
#include <fmt/format.h>

namespace commands::downloader::transcoder
{
static dpp::cluster* logger_bot{nullptr};  // For AV logger only!

dpp::loglevel avLogLevelToDpp(int level)
{
    switch (level)
    {
        case AV_LOG_QUIET:
            return dpp::ll_debug;
        case AV_LOG_PANIC:
            return dpp::ll_critical;
        case AV_LOG_FATAL:
            return dpp::ll_error;
        case AV_LOG_ERROR:
            return dpp::ll_error;
        case AV_LOG_WARNING:
            return dpp::ll_warning;
        case AV_LOG_INFO:
            return dpp::ll_info;
        case AV_LOG_VERBOSE:
            return dpp::ll_debug;
        case AV_LOG_DEBUG:
            return dpp::ll_debug;
        case AV_LOG_TRACE:
            return dpp::ll_trace;
        default:
            return dpp::ll_debug;
    }
}

static void avLogCallback(void*, int level, const char* fmt, va_list vl)
{
    if (logger_bot)
    {
        int size_s = snprintf(nullptr, 0, fmt, vl) + 1;
        if (size_s <= 0)
        {
            logger_bot->log(dpp::ll_error, "Failed to format AV log message");
            return;
        }
        auto size = static_cast<size_t>(size_s);
        std::unique_ptr<char[]> buf(new char[size]);
        snprintf(buf.get(), size, fmt, vl);
        logger_bot->log(avLogLevelToDpp(level),
                        "AV: " + std::string(buf.get(), buf.get() + size - 1));
    }
}

OpusTranscoder::OpusTranscoder(const std::string& inFileName,
                               const std::string& outFileName,
                               dpp::cluster* bot)
    : inFileName(inFileName), outFileName(outFileName), bot(bot)
{
    if (dpp::run_once<struct AvLog>())
    {
        logger_bot = bot;
        av_log_set_callback(avLogCallback);
    }
};

OpusTranscoder::Result OpusTranscoder::transcode()
{
    if (openInput() != 0)
    {
        return Result::fail;
    }

    if (openOutput() != 0)
    {
        return Result::fail;
    }

    if (initResampler() != 0)
    {
        return Result::fail;
    }

    if (fifo.alloc(outCodecCtx) < 0)
    {
        bot->log(dpp::ll_error, "Failed to allocate audio FIFO");
        return Result::fail;
    }
    if (writeOutputHeader() != 0)
    {
        return Result::fail;
    }

    return Result::success;
}

int OpusTranscoder::openInput()
{
    int error{0};

    error = inFormatCtx.openInput(inFileName);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to open input file: {}", inFileName));
        return error;
    }

    error = avformat_find_stream_info(inFormatCtx.get(), nullptr);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to find stream info: {}", inFileName));
        return error;
    }

    if (inFormatCtx->nb_streams != 1)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Input file has {} streams, expected 1",
                             inFormatCtx->nb_streams));
        return AVERROR_EXIT;
    }

    auto* stream = inFormatCtx->streams[0];

    auto* codec = avcodec_find_decoder(stream->codecpar->codec_id);
    if (!codec)
    {
        bot->log(dpp::ll_error, "Failed to find codec");
        return AVERROR_EXIT;
    }

    inCodecCtx.init(codec);
    if (not inCodecCtx.get())
    {
        bot->log(dpp::ll_error, "Failed to allocate codec context");
        return AVERROR_EXIT;
    }

    error = avcodec_parameters_to_context(inCodecCtx.get(), stream->codecpar);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to copy codec parameters: {}",
                             av_err2str(error)));
        return error;
    }

    error = avcodec_open2(inCodecCtx.get(), codec, nullptr);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to open codec: {}", av_err2str(error)));
        return error;
    }

    inCodecCtx->pkt_timebase = stream->time_base;
    return 0;
}

int OpusTranscoder::openOutput()
{
    int error{0};

    error = outFormatCtx.openOutput(outFileName);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to open output file: {}", outFileName));
        return error;
    }

    outFormatCtx->oformat =
        av_guess_format(nullptr, outFileName.c_str(), nullptr);
    if (!outFormatCtx->oformat)
    {
        bot->log(dpp::ll_error, "Failed to guess output format");
        return AVERROR_EXIT;
    }

    outFormatCtx->url = av_strdup(outFileName.c_str());
    if (!outFormatCtx->url)
    {
        bot->log(dpp::ll_error, "Failed to allocate output URL");
        return AVERROR(ENOMEM);
    }

    auto* codec = avcodec_find_encoder(AV_CODEC_ID_OPUS);
    if (!codec)
    {
        bot->log(dpp::ll_error, "Failed to find codec");
        return AVERROR_EXIT;
    }

    auto* stream = avformat_new_stream(outFormatCtx.get(), codec);
    if (!stream)
    {
        bot->log(dpp::ll_error, "Failed to allocate stream");
        return AVERROR(ENOMEM);
    }

    outCodecCtx.init(codec);
    if (not outCodecCtx.get())
    {
        bot->log(dpp::ll_error, "Failed to allocate codec context");
        return AVERROR_EXIT;
    }

    av_channel_layout_from_string(&(outCodecCtx->ch_layout), "stereo");

    outCodecCtx->sample_fmt = codec->sample_fmts[0];
    outCodecCtx->sample_rate = 48000;
    outCodecCtx->bit_rate = 96000;

    stream->time_base = {1, outCodecCtx->sample_rate};

    if (outFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
    {
        outCodecCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }

    error = avcodec_open2(outCodecCtx.get(), codec, nullptr);
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to open codec: {}", av_err2str(error)));
        return error;
    }

    error =
        avcodec_parameters_from_context(stream->codecpar, outCodecCtx.get());
    if (error < 0)
    {
        bot->log(dpp::ll_error,
                 fmt::format("Failed to copy codec parameters: {}",
                             av_err2str(error)));
        return error;
    }
    return 0;
}

int OpusTranscoder::initResampler()
{
    int error{0};

    error = resampleCtx.alloc(inCodecCtx, outCodecCtx);
    if (error < 0)
    {
        bot->log(dpp::ll_error, "Failed to allocate resampler context");
        return error;
    }

    error = swr_init(resampleCtx.get());
    if (error < 0)
    {
        bot->log(dpp::ll_error, "Failed to initialize resampler");
        return error;
    }
    return 0;
}

int OpusTranscoder::writeOutputHeader()
{
    int error{0};

    error = avformat_write_header(outFormatCtx.get(), nullptr);
    if (error < 0)
    {
        bot->log(dpp::ll_error, fmt::format("Failed to write output header: {}",
                                            av_err2str(error)));
        return error;
    }
    return 0;
}
}  // namespace commands::downloader::transcoder
