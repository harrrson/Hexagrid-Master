#include "commands/downloader/transcoder/transcoder.hpp"
extern "C"
{
#include <libavformat/avio.h>
}

namespace commands::downloader::transcoder
{

OpusTranscoder::InputAVFormatContext::InputAVFormatContext() : ctx(nullptr) {}

OpusTranscoder::InputAVFormatContext::~InputAVFormatContext()
{
    if (ctx)
    {
        avformat_close_input(&ctx);
    }
}

int OpusTranscoder::InputAVFormatContext::openInput(const std::string& fileName)
{
    return avformat_open_input(&ctx, fileName.c_str(), nullptr, nullptr);
}

AVFormatContext* OpusTranscoder::InputAVFormatContext::get()
{
    return ctx;
}

AVFormatContext* OpusTranscoder::InputAVFormatContext::operator->()
{
    return ctx;
}

OpusTranscoder::OutputAVFormatContext::OutputAVFormatContext()
{
    ctx = avformat_alloc_context();
}

OpusTranscoder::OutputAVFormatContext::~OutputAVFormatContext()
{
    if (ctx)
    {
        avio_closep(&ctx->pb);
        avformat_free_context(ctx);
    }
}

int OpusTranscoder::OutputAVFormatContext::openOutput(
    const std::string& fileName)
{
    return avio_open(&ctx->pb, fileName.c_str(), AVIO_FLAG_WRITE);
}

AVFormatContext* OpusTranscoder::OutputAVFormatContext::get()
{
    return ctx;
}

AVFormatContext* OpusTranscoder::OutputAVFormatContext::operator->()
{
    return ctx;
}

OpusTranscoder::SmartAVCodecContext::SmartAVCodecContext() : ctx(nullptr) {}

OpusTranscoder::SmartAVCodecContext::~SmartAVCodecContext()
{
    if (ctx)
    {
        avcodec_free_context(&ctx);
    }
}

void OpusTranscoder::SmartAVCodecContext::init(const AVCodec* codec)
{
    ctx = avcodec_alloc_context3(codec);
}

AVCodecContext* OpusTranscoder::SmartAVCodecContext::get()
{
    return ctx;
}

AVCodecContext* OpusTranscoder::SmartAVCodecContext::operator->()
{
    return ctx;
}

OpusTranscoder::SmartResampleContext::SmartResampleContext() : ctx(nullptr) {}

OpusTranscoder::SmartResampleContext::~SmartResampleContext()
{
    if (ctx)
    {
        swr_free(&ctx);
    }
}

int OpusTranscoder::SmartResampleContext::alloc(
    OpusTranscoder::SmartAVCodecContext& inCodecCtx,
    OpusTranscoder::SmartAVCodecContext& outCodecCtx)
{
    return swr_alloc_set_opts2(
        &ctx, &(outCodecCtx->ch_layout), outCodecCtx->sample_fmt,
        outCodecCtx->sample_rate, &(inCodecCtx->ch_layout),
        inCodecCtx->sample_fmt, inCodecCtx->sample_rate, 0, nullptr);
}

SwrContext* OpusTranscoder::SmartResampleContext::get()
{
    return ctx;
}

SwrContext* OpusTranscoder::SmartResampleContext::operator->()
{
    return ctx;
}

OpusTranscoder::SmartFifo::SmartFifo() : fifo(nullptr) {}

OpusTranscoder::SmartFifo::~SmartFifo()
{
    if (fifo)
    {
        av_audio_fifo_free(fifo);
    }
}

int OpusTranscoder::SmartFifo::alloc(
    OpusTranscoder::SmartAVCodecContext& outCodecCtx)
{
    fifo = av_audio_fifo_alloc(outCodecCtx->sample_fmt,
                               outCodecCtx->ch_layout.nb_channels, 1);
    return fifo ? 0 : AVERROR(ENOMEM);
}

AVAudioFifo* OpusTranscoder::SmartFifo::get()
{
    return fifo;
}

AVAudioFifo* OpusTranscoder::SmartFifo::operator->()
{
    return fifo;
}
}  // namespace commands::downloader::transcoder
