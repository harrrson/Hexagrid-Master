#include "commands/musicPlayer/playerManager.hpp"

#include <dpp/misc-enum.h>
#include <dpp/snowflake.h>
#include <fmt/core.h>

#include <atomic>
#include <mutex>
#include <set>
#include <thread>
#include <utility>

#include "commands/musicPlayer/player.hpp"
#include "commands/musicPlayer/queueManager.hpp"

namespace musicCommand::playerManager
{
using PlayerId = std::uint32_t;
using PlayerControllerMap =
    std::map<PlayerId, std::unique_ptr<player::PlayerController>>;
using PlayerThreadMap = std::map<PlayerId, std::thread>;
static PlayerControllerMap playerControllers{};
static PlayerThreadMap players{};
static std::map<dpp::snowflake, PlayerId> assignedPlayers{};
static std::set<PlayerId> freePlayers{};
static std::mutex playerControllersMut;
static std::mutex playersMut;
static std::atomic<PlayerId> unassignedId{0};

bool isPlayerAssigned(dpp::snowflake guildId)
{
    return assignedPlayers.contains(guildId);
}

PlayerId assignPlayerLocal(const dpp::slashcommand_t& event)
{
    PlayerId assignedPlayer{};
    auto guildId = event.command.guild_id;
    std::unique_lock lock(playersMut);
    if (not freePlayers.empty())
    {
        auto firstFreePlayer = freePlayers.begin();
        assignedPlayer = *firstFreePlayer;
        freePlayers.erase(firstFreePlayer);
        event.from->log(
            dpp::ll_debug,
            fmt::format("Available unassigned player {}", assignedPlayer));
    }
    else
    {
        assignedPlayer = unassignedId++;
        event.from->log(
            dpp::ll_debug,
            fmt::format("No free players are available, creating new player {}",
                        assignedPlayer));
        auto emplacedController = playerControllers.emplace(std::make_pair(
            assignedPlayer, std::make_unique<player::PlayerController>(
                                0, event.from->creator, nullptr)));

        players.emplace(std::make_pair(
            assignedPlayer,
            std::thread(&player::playerThread,
                        emplacedController.first->second.get())));
    }
    event.from->log(
        dpp::ll_debug,
        fmt::format("Assigning existing player to guild {}, playerId {}",
                    guildId, assignedPlayer));
    assignedPlayers[guildId] = assignedPlayer;
    return assignedPlayer;
}

void assignPlayer(const dpp::slashcommand_t& event)
{
    auto guildId = event.command.guild_id;
    if (isPlayerAssigned(guildId))
    {
        std::unique_lock lock{playerControllersMut};
        event.from->log(
            dpp::ll_debug,
            fmt::format("Guild {} already had assigned player, updating client",
                        guildId));
        auto& playerToUpdate = assignedPlayers.at(guildId);
        auto& controller = playerControllers.at(playerToUpdate);
        controller->client = event.from;
        return;
    }
    event.from->log(dpp::ll_debug,
                    fmt::format("Creating player for guild {}", guildId));
    queueManager::createQueueForGuild(event);

    PlayerId assignedPlayer = assignPlayerLocal(event);
    std::unique_lock lockCon(playerControllersMut);
    auto& controller = playerControllers.at(assignedPlayer);
    lockCon.unlock();
    std::unique_lock lockCv(controller->mutCv);
    controller->guildId = guildId;
    controller->client = event.from;
    lockCv.unlock();
    controller->cvWait.notify_one();
}

void releasePlayer(const dpp::slashcommand_t& event)
{
    auto guildId = event.command.guild_id;
    if (not isPlayerAssigned(guildId))
    {
        event.from->log(
            dpp::ll_debug,
            fmt::format(
                "Trying to release player for guild {}, but none were assigned",
                guildId));
        return;
    }
    std::unique_lock playersLock(playersMut);
    auto releasedPlayer = assignedPlayers.at(guildId);
    event.from->log(dpp::ll_debug,
                    fmt::format("Releasing player {} from guild {}",
                                releasedPlayer, guildId));
    assignedPlayers.erase(guildId);
    freePlayers.insert(releasedPlayer);
    playersLock.unlock();
    std::unique_lock lockCon(playerControllersMut);
    auto& controller = playerControllers.at(releasedPlayer);
    lockCon.unlock();
    std::unique_lock lockCv(controller->mutCv);
    controller->guildId = 0;
    lockCv.unlock();
    controller->cvWait.notify_one();
}

void notifyPlayerNewQueueElement(dpp::snowflake guildId)
{
    std::scoped_lock lock(playerControllersMut, playersMut);
    if (not isPlayerAssigned(guildId))
    {
        return;
    }
    auto& controllerId = assignedPlayers.at(guildId);
    auto& controller = playerControllers.at(controllerId);
    controller->cvWait.notify_one();
}
}  // namespace musicCommand::playerManager
