#include "commands/musicPlayer/player.hpp"

#include <dpp/misc-enum.h>
#include <dpp/utility.h>
#include <fmt/core.h>

#include <istream>
#include <mutex>

#include "commands/musicPlayer/queueManager.hpp"

namespace musicCommand::player
{
void playerThread(PlayerController* ctrl)
{
    dpp::utility::set_thread_name("Music player");
    ctrl->bot->log(dpp::ll_debug, "Created player thread");
    while (ctrl->alive)
    {
        if (ctrl->alive and (0 == ctrl->guildId))
        {
            ctrl->bot->log(dpp::ll_debug, "Awaiting for new assigned guild");
            // waiting for guild assign
            std::unique_lock wakeupLk(ctrl->mutCv);
            ctrl->cvWait.wait(
                wakeupLk,
                [&ctrl]() { return not ctrl->alive or (ctrl->guildId > 0); });
        }
        if (ctrl->alive and (ctrl->guildId > 0) and
            queueManager::isQueueEmpty(ctrl->guildId))
        {
            std::unique_lock lock(ctrl->mutCv);
            ctrl->cvWait.wait(
                lock,
                [ctrl]()
                {
                    return not ctrl->alive or (ctrl->guildId == 0) or
                           not queueManager::isQueueEmpty(ctrl->guildId);
                });
        }
        while (ctrl->alive and (ctrl->guildId > 0) and
               not queueManager::isQueueEmpty(ctrl->guildId))
        {
            ctrl->bot->log(dpp::ll_debug,
                           fmt::format("Getting queue element for guild: {}",
                                       ctrl->guildId));
            ctrl->bot->log(
                dpp::ll_debug,
                fmt::format("Is queue empty: {}",
                            queueManager::isQueueEmpty(ctrl->guildId)));
            auto& element = queueManager::getElement(ctrl->guildId);
            (void)element;

            // std::istream

            queueManager::popElement(ctrl->guildId,
                                     false);  // TODO: Add queue looping
        }
    }
    ctrl->bot->log(dpp::ll_debug, "Finished lifecycle of player");
}
}  // namespace musicCommand::player
