#include "commands/musicPlayer/queueManager.hpp"

#include <dpp/misc-enum.h>
#include <dpp/snowflake.h>
#include <fmt/format.h>

#include <deque>
#include <map>

#include "commands/musicPlayer/playerManager.hpp"

namespace musicCommand::queueManager
{

using Queue = std::deque<QueueElement>;
using GuildQueueMap = std::map<dpp::snowflake, Queue>;
static GuildQueueMap guildQueues{};
static std::mutex queueMut;

void createQueueForGuild(const dpp::slashcommand_t& event)
{
    std::unique_lock<std::mutex> queueLock(queueMut);
    guildQueues.insert({event.command.guild_id, Queue{}});
}

void addElementToQueue(dpp::snowflake guildId, QueueElement element)
{
    std::unique_lock queueLock(queueMut);
    auto& queue = guildQueues.at(guildId);
    queue.push_back(element);
    queueLock.unlock();
    playerManager::notifyPlayerNewQueueElement(guildId);
}

bool isQueueEmpty(dpp::snowflake guildId)
{
    std::unique_lock queueLock(queueMut);
    return guildQueues.at(guildId).empty();
}

QueueElement& getElement(dpp::snowflake guildId)
{
    std::unique_lock queueLock(queueMut);
    return guildQueues.at(guildId).front();
}

void popElement(dpp::snowflake guildId, bool reinsert)
{
    std::unique_lock queueLock(queueMut);
    auto& queue = guildQueues.at(guildId);
    if (reinsert)
    {
        queue.push_back(queue.front());
    }
    queue.pop_front();
}
}  // namespace musicCommand::queueManager
