#pragma once

#include <dpp/dpp.h>

#include <deque>
#include <mutex>

#include "lavalink/events.hpp"
#include "lavalink/types.hpp"

namespace lavalink
{
class LavalinkWebsocket;
class LavalinkConnector;

class PlayerController
{
    LavalinkWebsocket* ws;
    LavalinkConnector* owner;
    dpp::cluster* bot;
    dpp::snowflake guildId;

    dpp::snowflake voiceChannelId;
    VoiceState voiceState;
    bool connected{false};
    bool playing{false};
    bool paused{false};

    bool ignoreVoiceServerUpdate{false};

    dpp::snowflake messageIdForController;
    Track currentTrack;

   public:
    PlayerController(LavalinkWebsocket* ws_, LavalinkConnector* owner_,
                     dpp::cluster* bot_, dpp::snowflake guildId_);

    void onPlayerUpdate(const PlayerUpdateEvent& event);
    void onTrackStart(const TrackStartEvent& event);
    void onTrackEnd(const TrackEndEvent& event);
    void onTrackException(const TrackExceptionEvent& event);
    void onTrackStuck(const TrackStuckEvent& event);
    void onWebSocketClosed(const WebSocketClosedEvent& event);

    void onVoiceServerUpdate(const dpp::voice_server_update_t& event);

    bool isConnected() const;
    bool isPlaying() const;
    bool isPaused() const;

    dpp::task<bool> connect(const dpp::snowflake& channelId);
    dpp::task<bool> disconnect();
    dpp::snowflake getVoiceChannelId() const;
    dpp::task<bool> playFromLink(const std::string& link);
    dpp::task<bool> stop();
    dpp::task<bool> pause();
    dpp::task<bool> resume();

   private:
    dpp::task<bool> co_sendVoiceUpdate(nlohmann::json& d,
                                       bool noReplace = false);
    void sendVoiceUpdate(nlohmann::json& d, bool noReplace = false);
    void sendVoiceDelete();
};
}  // namespace lavalink
