#pragma once
#include <dpp/dpp.h>

#include <cassert>
#include <cstdint>
#include <string>

#include "events.hpp"
#include "lavalink/PlayerController.hpp"

namespace lavalink
{
class LavalinkWebsocket;

class LavalinkConnector
{
    friend class PlayerController;
    std::string host;
    std::uint32_t port;
    dpp::cluster* bot;
    std::string hostUrl;
    std::pair<std::string, std::string> authHeader;
    std::string apiVersion;
    LavalinkWebsocket* ws;

    std::mutex connectedPlayersMutex;
    std::map<dpp::snowflake, std::unique_ptr<PlayerController>>
        playerControllers;

   public:
    LavalinkConnector(dpp::cluster* bot_, const std::string& ip,
                      std::uint32_t port, const std::string& password,
                      const std::string& apiVerison = "v4");
    ~LavalinkConnector() = default;

    dpp::task<ResponseCallback> co_getLavalinkInfo();
    dpp::task<ResponseCallback> co_getLavalinkVersion();
    dpp::task<ResponseCallback> co_getLavalinkStats();
    dpp::task<ResponseCallback> co_getPlayer(const dpp::snowflake& guildId);
    dpp::task<ResponseCallback> co_getPlayers();

    PlayerController* getPlayerController(const dpp::snowflake& guildId);

    uint32_t shardIdForGuild(const dpp::snowflake& guildId);

    dpp::event_router_t<PlayerUpdateEvent> onPlayerUpdate;
    dpp::event_router_t<StatsEvent> onStats;
    dpp::event_router_t<TrackStartEvent> onTrackStart;
    dpp::event_router_t<TrackEndEvent> onTrackEnd;
    dpp::event_router_t<TrackExceptionEvent> onTrackException;
    dpp::event_router_t<TrackStuckEvent> onTrackStuck;
    dpp::event_router_t<WebSocketClosedEvent> onWebSocketClosed;

    void run();

   private:
    dpp::task<dpp::http_request_completion_t> requestGet(
        const std::string& endpoint);

    std::string sessionEndpoint();
};
}  // namespace lavalink
