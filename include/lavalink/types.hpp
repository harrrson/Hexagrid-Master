#pragma once

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace lavalink
{

template <typename T>
void from_json(const nlohmann::json& j, std::optional<T>& opt)
{
    if (not j.is_null())
    {
        opt.emplace();
        j.get_to(opt.value());
    }
}

using Timestamp = std::uint64_t;

struct TrackInfo
{
    std::string identifier{};
    bool isSeekable{};
    std::string author{};
    Timestamp length{};
    bool isStream{};
    Timestamp position{};
    std::string title{};
    std::optional<std::string> uri{};
    std::optional<std::string> artworkUrl{};
    std::optional<std::string> isrc{};
    std::string sourceName{};
};

void from_json(const nlohmann::json& j, TrackInfo& info);

struct Track
{
    std::string encoded{};
    TrackInfo info{};
    // Object pluginInfo
    // Object userData
};

void from_json(const nlohmann::json& j, Track& info);

struct PlaylistInfo
{
    std::string name{};
    std::int16_t selectedTrack{};
};

enum class LoadResultType
{
    track,
    playlist,
    search,
    empty,
    error
};

enum class Severity
{
    common,
    suspicious,
    fault
};

void from_json(const nlohmann::json& j, Severity& info);

struct Exception
{
    std::optional<std::string> message{};
    Severity severity{};
    std::string cause{};
};

void from_json(const nlohmann::json& j, Exception& info);

using TrackArray = std::vector<Track>;

using LoadResultData =
    std::variant<std::monostate, Track, PlaylistInfo, TrackArray, Exception>;

struct TrackLoadingResult
{
    LoadResultType loadType{};
    LoadResultData data{};
};

struct PlayerState
{
    Timestamp time{};
    Timestamp position{};
    bool connected{};
    std::int32_t ping{};
};

void from_json(const nlohmann::json& j, PlayerState& info);

struct VoiceState
{
    std::string token{};
    std::string endpoint{};
    std::string sessionId{};
};

void from_json(const nlohmann::json& j, VoiceState& info);

void to_json(nlohmann::json& j, const VoiceState& info);

struct Equalizer
{
    int band{};
    float gain{};
};

void from_json(const nlohmann::json& j, Equalizer& info);

struct Karaoke
{
    std::optional<float> level{};
    std::optional<float> monoLevel{};
    std::optional<float> filterBand{};
    std::optional<float> filterWidth{};
};

void from_json(const nlohmann::json& j, Karaoke& info);

struct Timescale
{
    std::optional<float> speed{};
    std::optional<float> pitch{};
    std::optional<float> rate{};
};

void from_json(const nlohmann::json& j, Timescale& info);

struct Tremolo
{
    std::optional<float> frequency{};
    std::optional<float> depth{};
};

void from_json(const nlohmann::json& j, Tremolo& info);

struct Vibrato
{
    std::optional<float> frequency{};
    std::optional<float> depth{};
};

void from_json(const nlohmann::json& j, Vibrato& info);

struct Rotation
{
    std::optional<float> rotationHz{};
};

void from_json(const nlohmann::json& j, Rotation& info);

struct Distortion
{
    std::optional<float> sinOffset{};
    std::optional<float> sinScale{};
    std::optional<float> cosOffset{};
    std::optional<float> cosScale{};
    std::optional<float> tanOffset{};
    std::optional<float> tanScale{};
    std::optional<float> offset{};
    std::optional<float> scale{};
};

void from_json(const nlohmann::json& j, Distortion& info);

struct ChannelMix
{
    std::optional<float> leftToLeft{};
    std::optional<float> leftToRight{};
    std::optional<float> rightToLeft{};
    std::optional<float> rightToRight{};
};

void from_json(const nlohmann::json& j, ChannelMix& info);

struct LowPass
{
    std::optional<float> smoothing{};
};

void from_json(const nlohmann::json& j, LowPass& info);

using PluginFilters = nlohmann::json;

struct Filters
{
    std::optional<float> volume{};
    std::vector<Equalizer> equalizer{};
    std::optional<Karaoke> karaoke{};
    std::optional<Timescale> timescale{};
    std::optional<Tremolo> tremolo{};
    std::optional<Vibrato> vibrato{};
    std::optional<Rotation> rotation{};
    std::optional<Distortion> distortion{};
    std::optional<ChannelMix> channelMix{};
    std::optional<LowPass> lowPass{};
    std::map<std::string, PluginFilters> pluginFilters{};
};

void from_json(const nlohmann::json& j, Filters& info);

struct Player

{
    std::string guildId{};
    std::optional<Track> track{};
    std::uint16_t volume{};
    bool paused{};
    PlayerState state{};
    VoiceState voice{};
    Filters filters{};
};

void from_json(const nlohmann::json& j, Player& info);

using PlayerList = std::vector<Player>;

struct UpdatePlayerTrack
{
    std::string encoded{};
    std::string identifier{};
    // Object userData
};

void to_json(nlohmann::json& j, const UpdatePlayerTrack& info);

struct Version
{
    std::string semver{};
    std::uint16_t major{};
    std::uint16_t minor{};
    std::uint16_t patch{};
    std::string preRelease{};
    std::string build{};
};

void from_json(const nlohmann::json& j, Version& info);

struct Git
{
    std::string branch{};
    std::string commit{};
    Timestamp commitTime{};
};

void from_json(const nlohmann::json& j, Git& info);

struct Plugin
{
    std::string name{};
    std::string version{};
};

void from_json(const nlohmann::json& j, Plugin& info);

struct InfoResponse
{
    Version version{};
    Timestamp buildTime{};
    Git git{};
    std::string jvm{};
    std::string lavaplayer{};
    std::vector<std::string> sourceManagers{};
    std::vector<std::string> filters{};
    std::vector<Plugin> plugins{};
};

void from_json(const nlohmann::json& j, InfoResponse& info);

struct Memory
{
    std::uint64_t free{};
    std::uint64_t used{};
    std::uint64_t allocated{};
    std::uint64_t reservable{};
};

void from_json(const nlohmann::json& j, Memory& info);

struct CPU
{
    std::uint16_t cores{};
    float systemLoad{};
    float lavalinkLoad{};
};

void from_json(const nlohmann::json& j, CPU& info);

struct FrameStats
{
    std::uint64_t sent{};
    std::uint64_t nulled{};
    std::uint64_t deficit{};
};

void from_json(const nlohmann::json& j, FrameStats& info);

struct Stats
{
    std::uint16_t players{};
    std::uint16_t playingPlayers{};
    Timestamp uptime{};
    Memory memory{};
    CPU cpu{};
    std::optional<FrameStats> frameStats{};
};

void from_json(const nlohmann::json& j, Stats& info);

using ResponseData = std::variant<std::monostate, InfoResponse, Stats,
                                  std::string, Player, PlayerList>;

struct ResponseCallback
{
    bool succesfull;
    ResponseData result;
};

enum class TrackEndReason
{
    finished,
    loadFailed,
    stopped,
    replaced,
    cleanup
};

void from_json(const nlohmann::json& j, TrackEndReason& info);

}  // namespace lavalink
