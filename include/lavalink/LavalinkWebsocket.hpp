#pragma once

#include <dpp/dpp.h>

#include <atomic>
#include <string>
#include <thread>

namespace lavalink
{
class LavalinkConnector;

enum wsState : uint8_t
{
    httpHeaders,
    connected
};

enum ws_opcode : uint8_t
{
    /**
     * @brief Continuation.
     */
    OP_CONTINUATION = 0x00,

    /**
     * @brief Text frame.
     */
    OP_TEXT = 0x01,

    /**
     * @brief Binary frame.
     */
    OP_BINARY = 0x02,

    /**
     * @brief Close notification with close code.
     */
    OP_CLOSE = 0x08,

    /**
     * @brief Low level ping.
     */
    OP_PING = 0x09,

    /**
     * @brief Low level pong.
     */
    OP_PONG = 0x0a
};

class LavalinkWebsocket : dpp::ssl_client
{
    std::string key;
    wsState state;
    std::string path;
    ws_opcode dataOpcode;
    std::map<std::string, std::string> http_headers;

    std::string password;
    std::string sessionId{};

    std::thread *runner{nullptr};
    std::atomic<bool> terminating{false};
    LavalinkConnector *owner{nullptr};
    dpp::cluster *bot{nullptr};

   public:
    LavalinkWebsocket(const std::string &hostname, const std::string &port,
                      const std::string &password, LavalinkConnector *owner_,
                      dpp::cluster *bot_);

    ~LavalinkWebsocket();

    void run();

    void onReady(nlohmann::json &data);
    void onPlayerUpdate(nlohmann::json &data);
    void onStats(nlohmann::json &data);
    void onEvent(nlohmann::json &data);

    std::string getSessionId() const;

   private:
    void connect();
    void runLoop();
    void cleanup();
    virtual bool handle_frame(const std::string &buffer);
    void send_close_packet();
    size_t fill_header(unsigned char *outbuf, size_t sendlength,
                       ws_opcode opcode);
    void write(const std::string &data);
    bool handle_buffer(std::string &buffer);
    void log(dpp::loglevel severity, const std::string &msg) const;
    bool parseheader(std::string &data);
    void one_second_timer();
    void handle_ping_pong(bool ping, const std::string &payload);
    void error(uint32_t errorcode);
    void close();
};
}  // namespace lavalink
