#pragma once

#include <dpp/dpp.h>

#include "lavalink/types.hpp"

namespace lavalink
{
class LavalinkConnector;

struct DispatchedEvent
{
    LavalinkConnector* connector;
    dpp::cluster* bot;
    mutable bool cancelled{false};

    const DispatchedEvent& cancel_event() const;
    DispatchedEvent cancel_event();

    bool is_cancelled() const;
};

struct PlayerUpdateEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    PlayerState state;
};

struct StatsEvent : public DispatchedEvent
{
    Stats stats;
};

struct TrackStartEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    Track track;
};

struct TrackEndEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    Track track;
    TrackEndReason reason;
};

struct TrackExceptionEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    Track track;
    Exception error;
};

struct TrackStuckEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    Track track;
    std::uint64_t threshold;
};

struct WebSocketClosedEvent : public DispatchedEvent
{
    dpp::snowflake guildId;
    std::uint16_t code;
    std::string reason;
    bool byRemote;
};
}  // namespace lavalink
