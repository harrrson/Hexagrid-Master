#pragma once
#include <dpp/dpp.h>
#include <fmt/format.h>

#include <string>
#include <unordered_map>

namespace command
{
struct Slashcommand
{
    static constexpr std::string_view name{};
    static dpp::slashcommand registerCommand(dpp::cluster&);
    static dpp::task<void> route(const dpp::slashcommand_t&);
};

struct Messagecommand
{
    static constexpr std::string_view name{};
    static void route(const dpp::message_create_t&, const std::string&);
};

using MessageCommandRouter = auto (*)(const dpp::message_create_t&,
                                      const std::string&) -> void;
using SlashCommandRouter = auto (*)(const dpp::slashcommand_t&)
    -> dpp::task<void>;

using MessageCommandList =
    std::unordered_map<std::string_view, MessageCommandRouter>;
using SlashCommandList =
    std::unordered_map<std::string_view, SlashCommandRouter>;

MessageCommandList& getMessageCommands();
SlashCommandList& getSlashCommands();

template <typename T>
dpp::slashcommand registerSlashCommand(dpp::cluster& bot)
{
    auto& commands = getSlashCommands();
    commands[T::name] = &T::route;
    bot.log(dpp::ll_debug,
            fmt::format("Registered slash command '{}'", T::name));
    return T::registerCommand(bot);
}

template <typename T>
void registerMessageCommand(dpp::cluster& bot)
{
    auto& commands = getMessageCommands();
    commands[T::name] = &T::route;
    bot.log(dpp::ll_debug,
            fmt::format("Registered message command '{}'", T::name));
}

dpp::task<void> routeCommand(const dpp::slashcommand_t&);
void routeMessage(const std::string&, const dpp::message_create_t&);
}  // namespace command
