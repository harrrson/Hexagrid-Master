#pragma once
#include <dpp/dpp.h>

namespace listeners
{
void on_ready(const dpp::ready_t&);
void on_message(const dpp::message_create_t&);
dpp::task<void> on_slashcommand(const dpp::slashcommand_t&);
}  // namespace listeners
