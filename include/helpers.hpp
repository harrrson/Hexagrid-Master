#pragma once
#include <dpp/dpp.h>

#include <format>
#include <string>
#include <variant>
#include <vector>

namespace std
{
template <>
struct formatter<dpp::snowflake>
{
    template <class ParseContext>
    constexpr ParseContext::iterator parse(ParseContext &ctx)
    {
        return ctx.begin();
    }

    template <class FmtContext>
    FmtContext::iterator format(dpp::snowflake s, FmtContext &ctx) const
    {
        return format_to(ctx.out(), "{}", s.str());
    }
};
}  // namespace std

namespace helpers
{
using StringPair = std::pair<std::string, std::string>;
StringPair splitOnFirstOf(const std::string &, const std::string &);
std::vector<std::string> tokenize(const std::string &);
uint32_t shardIdForGuild(const dpp::snowflake &guildId, uint32_t numShards);

template <typename T>
[[nodiscard]] T getParamFromEvent(const dpp::slashcommand_t &event,
                                  const std::string &param)
{
    return std::get<T>(event.get_parameter(param));
}

template <typename T>
[[nodiscard]] T getOptionalParamFromEvent(const dpp::slashcommand_t &event,
                                          const std::string &param,
                                          T defaultVal)
{
    auto paramFromEvent = event.get_parameter(param);
    return std::holds_alternative<T>(paramFromEvent)
               ? std::get<T>(paramFromEvent)
               : defaultVal;
}

template <typename T>
[[nodiscard]] bool checkOptionalParamInEvent(const dpp::slashcommand_t &event,
                                             const std::string &param)
{
    return std::holds_alternative<T>(event.get_parameter(param));
}

template <typename Event>
void sendErrorEmbed(const std::string &error, const Event &event)
{
    dpp::embed embed{};
    embed.set_title("Error").set_description(error).set_color(
        dpp::utility::rgb(255, 0, 0));
    event.reply(dpp::message{}.add_embed(embed).set_flags(dpp::m_ephemeral));
}

template <typename Event, typename Predicate>
dpp::task<Event> awaitWhen(dpp::event_router_t<Event> &router,
                           Predicate &&predicate)
{
    co_return co_await router.when(predicate);
}

}  // namespace helpers
