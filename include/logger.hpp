#pragma once
#include <dpp/dpp.h>

namespace logger
{
void initLogger();
void log(const dpp::log_t&);
}  // namespace logger
