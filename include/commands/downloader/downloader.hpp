#pragma once

#include <dpp/dpp.h>

#include <string>

namespace commands::downloader
{
struct DownloadResult
{
    bool succesfull{};
};
enum class FileType
{
    generic,
    audio
};

void init(dpp::cluster* bot_);
void shutdown();
dpp::async<DownloadResult> downloadFile(const std::string& url,
                                        const std::string& filePath,
                                        const std::string& fileName,
                                        FileType fileType);
}  // namespace commands::downloader
