#pragma once

#include <dpp/dpp.h>

#include <string>

namespace commands::downloader
{
enum class Result
{
    success,
    fail
};

Result transcodeToOpus(const std::string& inFile, const std::string& outFile);
void initTranscoder(dpp::cluster* bot_);
}  // namespace commands::downloader
