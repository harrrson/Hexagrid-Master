#pragma once
#include "command.hpp"

namespace rollCommand
{
struct RollMsgCommand : public command::Messagecommand
{
    static constexpr std::string_view name{"roll"};
    static void route(const dpp::message_create_t&, const std::string&);
};

struct RollSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"roll"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct ColorSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"colorroll"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct FateSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"fateroll"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct DuelSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"duelroll"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};
}  // namespace rollCommand
