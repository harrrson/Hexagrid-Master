#pragma once
#include "command.hpp"

namespace infoCommand
{
struct InfoCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"info"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};
}  // namespace infoCommand
