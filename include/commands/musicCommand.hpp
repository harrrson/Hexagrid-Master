#pragma once
#include "command.hpp"

namespace musicCommand
{
void initCommandData(dpp::cluster*);

struct PlaySlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"play"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct StopSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"stop"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct PauseSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"pause"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct ResumeSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"resume"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct JoinSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"join"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct LeaveSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"leave"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};

struct TestSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"test"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};
}  // namespace musicCommand
