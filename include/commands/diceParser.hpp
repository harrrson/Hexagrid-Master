#pragma once
#include <array>
#include <cstdint>
#include <string>

namespace diceParser
{
struct Dice
{
    std::uint8_t count{1};
    std::uint32_t size{10};
    std::uint32_t modifier{0};
    char modifierType{'+'};
};

enum class ErrorCause
{
    NO_ERROR = 0,
    DICE_COUNT_OUT_OF_RANGE,
    DICE_SIZE_OUT_OF_RANGE,
    DICE_MOD_OUT_OF_RANGE,
    DIV_BY_0,
    IMPROPER_COMMAND_FORMAT
};

struct ParseResult
{
    Dice dice;
    ErrorCause errorCause;

    inline bool wasSuccesfull()
    {
        return ErrorCause::NO_ERROR == errorCause;
    }
};

ParseResult parseDice(const std::string&);

std::string getErrorMessage(ErrorCause);

std::array<std::uint32_t, 5> getLimits();

#ifdef HEXA_TESTING
void setLimits(int, int, int, int, int);
#endif
}  // namespace diceParser
