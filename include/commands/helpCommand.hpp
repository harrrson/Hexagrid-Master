#pragma once
#include "command.hpp"

namespace helpCommand
{
struct HelpMsgCommand : public command::Messagecommand
{
    static constexpr std::string_view name{"help"};
    static void route(const dpp::message_create_t&, const std::string&);
};

struct HelpSlashCommand : public command::Slashcommand
{
    static constexpr std::string_view name{"help"};
    static dpp::task<void> route(const dpp::slashcommand_t&);
    static dpp::slashcommand registerCommand(dpp::cluster&);
};
}  // namespace helpCommand
