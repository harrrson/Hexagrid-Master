#pragma once
#include <dpp/dpp.h>

#include <string>

namespace config
{
using json = nlohmann::json;

void initConfig(const std::string &);

json get(const std::string &);
json getOptional(const std::string &, json);
}  // namespace config
