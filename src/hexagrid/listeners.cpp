#include "listeners.hpp"

#include <dpp/dpp.h>
#include <fmt/format.h>

#include "command.hpp"
#include "commands/helpCommand.hpp"
#include "commands/infoCommand.hpp"
#include "commands/musicCommand.hpp"
#include "commands/rollCommand.hpp"
#include "config.hpp"

namespace listeners
{
static std::string prefix;

constexpr dpp::snowflake testGuildId = 1041388807976648725;

namespace
{
inline bool isInDevMode()
{
    return config::getOptional("devMode", false);
}
}  // namespace

void registerMsgCommands(dpp::cluster& bot)
{
    using command::registerMessageCommand;
    registerMessageCommand<rollCommand::RollMsgCommand>(bot);
    registerMessageCommand<helpCommand::HelpMsgCommand>(bot);
}

void registerSlashCommands(dpp::cluster& bot)
{
    // bot.global_bulk_command_delete_sync();
    using command::registerSlashCommand;
    if (isInDevMode()) [[unlikely]]
    {
        // Commands for testing purposes
        bot.guild_bulk_command_create(
            {
                registerSlashCommand<rollCommand::RollSlashCommand>(bot),
                registerSlashCommand<rollCommand::ColorSlashCommand>(bot),
                registerSlashCommand<rollCommand::FateSlashCommand>(bot),
                registerSlashCommand<rollCommand::DuelSlashCommand>(bot),
                registerSlashCommand<helpCommand::HelpSlashCommand>(bot),
                registerSlashCommand<musicCommand::JoinSlashCommand>(bot),
                registerSlashCommand<musicCommand::LeaveSlashCommand>(bot),
                registerSlashCommand<musicCommand::PlaySlashCommand>(bot),
                registerSlashCommand<musicCommand::StopSlashCommand>(bot),
                registerSlashCommand<musicCommand::PauseSlashCommand>(bot),
                registerSlashCommand<musicCommand::ResumeSlashCommand>(bot),
                registerSlashCommand<infoCommand::InfoCommand>(bot),
            },
            testGuildId);
    }
    else [[likely]]
    {
        // Commands for release
        bot.global_bulk_command_create({
            registerSlashCommand<rollCommand::RollSlashCommand>(bot),
            registerSlashCommand<rollCommand::ColorSlashCommand>(bot),
            registerSlashCommand<rollCommand::FateSlashCommand>(bot),
            registerSlashCommand<rollCommand::DuelSlashCommand>(bot),
            registerSlashCommand<helpCommand::HelpSlashCommand>(bot),
        });
    }
}

void deleteAllCommands(dpp::cluster& bot)
{
    bot.global_bulk_command_delete();
    bot.guild_bulk_command_delete(testGuildId);
}

void on_ready(const dpp::ready_t& event)
{
    dpp::cluster& bot = *event.from->creator;
    if (dpp::run_once<struct registerCommands>())
    {
        musicCommand::initCommandData(event.from->creator);
        prefix = config::get("prefix");
        bot.log(dpp::ll_debug, "Bot ready");
        if (isInDevMode()) [[unlikely]]
        {
            // deleteAllCommands(bot);
        }
        registerMsgCommands(bot);
        registerSlashCommands(bot);
    }
}

void on_message(const dpp::message_create_t& event)
{
    auto message = event.msg.content;
    if (event.msg.author.is_bot() or event.msg.author.id.empty())
    {
        return;
    }
    if (not message.starts_with(prefix))
    {
        event.from->log(dpp::ll_trace, "Did not find prefix in message");
        return;
    }
    event.from->log(dpp::ll_debug, "Command was invoked.");
    message.erase(0, prefix.size());
    command::routeMessage(message, event);
}

dpp::task<void> on_slashcommand(const dpp::slashcommand_t& event)
{
    co_await command::routeCommand(event);
    co_return;
}
}  // namespace listeners
