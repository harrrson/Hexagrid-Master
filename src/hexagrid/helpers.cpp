#include "helpers.hpp"

#include <sstream>

namespace helpers
{
StringPair splitOnFirstOf(const std::string& str, const std::string& sep)
{
    auto pos = str.find_first_of(sep);
    if (str.npos == pos)
    {
        return {str, ""};
    }
    return {str.substr(0, pos), str.substr(pos + 1)};
}

std::vector<std::string> tokenize(const std::string& in)
{
    std::vector<std::string> tokens{};
    std::stringstream ss{in};
    std::string token;
    while (ss >> token)
    {
        tokens.push_back(token);
    }
    return tokens;
}

uint32_t shardIdForGuild(const dpp::snowflake& guildId, uint32_t numShards)
{
    return (guildId >> 22) % numShards;
}
}  // namespace helpers
