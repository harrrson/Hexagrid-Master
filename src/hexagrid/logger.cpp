#include "logger.hpp"

#include <spdlog/async.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <string>

#include "config.hpp"

namespace logger
{
using spdlogLevel = spdlog::level::level_enum;

constexpr int maxLogSize = 1024 * 1024 * 5;

static std::shared_ptr<spdlog::logger> asyncLogger;

void initLogger()
{
    std::string logFile{config::get("logFile")};
    spdlog::init_thread_pool(8192, 2);
    std::vector<spdlog::sink_ptr> sinks = {
        std::make_shared<spdlog::sinks::stdout_color_sink_mt>(),
        std::make_shared<spdlog::sinks::rotating_file_sink_mt>(logFile,
                                                               maxLogSize, 10)};

    asyncLogger = std::make_shared<spdlog::async_logger>(
        "file_and_console", sinks.begin(), sinks.end(), spdlog::thread_pool(),
        spdlog::async_overflow_policy::block);

    asyncLogger->set_pattern("%^%Y-%m-%d %H:%M:%S.%e [%L] [th#%t]%$ : %v");
    int logLevelNumber = config::get("logLevel");
    auto logLevel = static_cast<spdlogLevel>(logLevelNumber);
    asyncLogger->set_level(logLevel);
    spdlog::register_logger(asyncLogger);

    asyncLogger->info("Initalized logger with {} log level.",
                      to_string_view(logLevel));
}

void log(const dpp::log_t& event)
{
    switch (event.severity)
    {
        case dpp::ll_trace:
            asyncLogger->trace("{}", event.message);
            break;
        case dpp::ll_debug:
            asyncLogger->debug("{}", event.message);
            break;
        case dpp::ll_info:
            asyncLogger->info("{}", event.message);
            break;
        case dpp::ll_warning:
            asyncLogger->warn("{}", event.message);
            break;
        case dpp::ll_error:
            asyncLogger->error("{}", event.message);
            break;
        case dpp::ll_critical:
            asyncLogger->critical("{}", event.message);
            break;
    }
}
}  // namespace logger
