#include "config.hpp"

#include <fstream>

namespace config
{
static json jsonConfig;

void initConfig(const std::string &path)
{
    std::ifstream configFile(path, std::ios::in);
    if (not configFile.is_open())
    {
        throw(std::runtime_error{"Cannot open config file '" + path + "'!"});
    }
    configFile >> jsonConfig;
}

json get(const std::string &param)
{
    return jsonConfig[param];
}

json getOptional(const std::string &param, json defaultValue)
{
    return jsonConfig.contains(param) ? jsonConfig[param] : defaultValue;
}
}  // namespace config
