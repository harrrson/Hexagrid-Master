#include "command.hpp"

#include <dpp/misc-enum.h>
#include <fmt/format.h>

#include "helpers.hpp"

namespace command
{
static MessageCommandList messageCommands;
static SlashCommandList slashCommands;

MessageCommandList& getMessageCommands()
{
    return messageCommands;
}

SlashCommandList& getSlashCommands()
{
    return slashCommands;
}

dpp::task<void> routeCommand(const dpp::slashcommand_t& event)
{
    auto ref = slashCommands.find(event.command.get_command_name());
    if (slashCommands.end() == ref)
    {
        event.reply(
            fmt::format("Fatal error: did not find command {},please contact "
                        "bot developer.",
                        event.command.get_command_name()));
        event.from->log(dpp::ll_error,
                        fmt::format("Tried to invoke non-existent command: {}",
                                    event.command.get_command_name()));
        co_return;
    }
    event.from->log(
        dpp::ll_debug,
        fmt::format("Routing command: {}", event.command.get_command_name()));
    co_await ref->second(event);
}

void routeMessage(const std::string& msg, const dpp::message_create_t& event)
{
    auto [command, restOfMessage] = helpers::splitOnFirstOf(msg, " \f\n\r\t\v");
    restOfMessage = dpp::ltrim(restOfMessage);
    event.from->log(dpp::ll_debug,
                    fmt::format("command: '{}', restOfMessage: '{}'", command,
                                restOfMessage));
    auto ref = messageCommands.find(command);
    if (messageCommands.end() == ref)
    {
        event.reply(fmt::format("No command named '{}'", command));
        event.from->log(
            dpp::ll_debug,
            fmt::format("Tried to invoke non-existent command: {}", command));
        return;
    }
    event.from->log(dpp::ll_debug, fmt::format("Invoked command: {}", command));
    ref->second(event, restOfMessage);
}
}  // namespace command
