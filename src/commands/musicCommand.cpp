#include "commands/musicCommand.hpp"

#include <dpp/dpp.h>
// #include <fmt/core.h>

#include <format>
#include <memory>

#include "config.hpp"
#include "helpers.hpp"
#include "lavalink/LavalinkConnector.hpp"

namespace musicCommand
{
std::unique_ptr<lavalink::LavalinkConnector> lavalinkConnector{nullptr};

lavalink::PlayerController *verifyIfCanUseCommand(
    const dpp::slashcommand_t &event)
{
    auto guildId = event.command.guild_id;
    auto guild = dpp::find_guild(guildId);
    if (not guild)
    {
        helpers::sendErrorEmbed(
            "Internal error: cannot find guild's data. Try again, and if this "
            "error persists contact bot's developer.",
            event);
        event.from->log(
            dpp::ll_error,
            std::format("Could not find guild in cache: {}", guildId));
        return nullptr;
    }
    auto player = lavalinkConnector->getPlayerController(guildId);
    if (not player->isConnected())
    {
        helpers::sendErrorEmbed("Bot is not connected to any voice channel.",
                                event);
        return nullptr;
    }

    auto voiceChannelForUser = guild->voice_members.find(event.command.usr.id);
    if ((guild->voice_members.end() == voiceChannelForUser) or
        (player->getVoiceChannelId() != voiceChannelForUser->second.channel_id))
    {
        helpers::sendErrorEmbed(
            "You need to be in voice channel with bot to use this command.",
            event);
        return nullptr;
    }
    return player;
}

void initCommandData(dpp::cluster *bot)
{
    bot->log(dpp::ll_info, "Initializing lavalink connector.");
    auto lavalinkConfig = config::get("lavalink");
    lavalinkConnector = std::make_unique<lavalink::LavalinkConnector>(
        bot, lavalinkConfig["host"], lavalinkConfig["port"],
        lavalinkConfig["password"]);
    lavalinkConnector->run();
}

dpp::slashcommand PlaySlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{PlaySlashCommand::name},
                             "Play music from selected source", bot.me.id)
        .add_option(dpp::command_option(dpp::co_attachment, "file",
                                        "Audio file which should be played"))
        .add_option(dpp::command_option(
            dpp::co_string, "link",
            "Link to the audio. Supported sources: Youtube"));
}

dpp::slashcommand StopSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{StopSlashCommand::name},
                             "Stop currently played music", bot.me.id);
}

dpp::slashcommand PauseSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{PauseSlashCommand::name},
                             "Pause music", bot.me.id);
}

dpp::slashcommand ResumeSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{ResumeSlashCommand::name},
                             "Resume playing music", bot.me.id);
}

dpp::slashcommand JoinSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{JoinSlashCommand::name},
                             "Bot joins your voicechannel", bot.me.id);
}

dpp::slashcommand LeaveSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{LeaveSlashCommand::name},
                             "Bot leaves voicechannel", bot.me.id);
}

bool checkIfLinkIsSupported(const std::string &link)
{
    if (link.starts_with("https://www.youtube.com/watch?v=") or
        link.starts_with("https://youtu.be/"))
    {
        return true;
    }
    return false;
}

dpp::task<void> PlaySlashCommand::route(const dpp::slashcommand_t &event)
{
    const auto player = verifyIfCanUseCommand(event);
    if (not player)
    {
        co_return;
    }

    // TODO: remove when queue is implemented
    if (player->isPlaying())
    {
        co_return helpers::sendErrorEmbed("Bot is already playing music.",
                                          event);
    }

    dpp::async thinking = event.co_thinking();
    if (helpers::checkOptionalParamInEvent<std::string>(event, "link"))
    {
        auto link = helpers::getParamFromEvent<std::string>(event, "link");
        if (not checkIfLinkIsSupported(link))
        {
            co_await thinking;
            co_return event.edit_original_response(
                dpp::message("Please provide link from supported source."));
        }
        co_await player->playFromLink(link);
        co_await thinking;
        co_return event.edit_original_response(
            dpp::message("Playing music from link."));
    }

    co_return;
}

dpp::task<void> StopSlashCommand::route(const dpp::slashcommand_t &event)
{
    const auto player = verifyIfCanUseCommand(event);
    if (not player)
    {
        co_return;
    }
    dpp::async thinking = event.co_thinking();
    co_await player->stop();
    co_await thinking;
    event.edit_original_response(dpp::message("Stopped playing music."));

    co_return;
}

dpp::task<void> PauseSlashCommand::route(const dpp::slashcommand_t &event)
{
    const auto player = verifyIfCanUseCommand(event);
    if (not player)
    {
        co_return;
    }

    if (not player->isPlaying())
    {
        co_return helpers::sendErrorEmbed(
            "Bot is not currently playing anything.", event);
    }

    if (player->isPaused())
    {
        co_return helpers::sendErrorEmbed("Bot is already paused.", event);
    }

    dpp::async thinking = event.co_thinking();
    co_await player->pause();
    co_await thinking;
    event.edit_original_response(dpp::message("Music paused."));

    co_return;
}

dpp::task<void> ResumeSlashCommand::route(const dpp::slashcommand_t &event)
{
    const auto player = verifyIfCanUseCommand(event);
    if (not player)
    {
        co_return;
    }

    if (not player->isPlaying())
    {
        co_return helpers::sendErrorEmbed(
            "Bot is not currently playing anything.", event);
    }

    if (not player->isPaused())
    {
        co_return helpers::sendErrorEmbed("Music is not paused.", event);
    }

    dpp::async thinking = event.co_thinking();
    co_await player->resume();
    co_await thinking;
    event.edit_original_response(dpp::message("Music resumed."));

    co_return;
}

dpp::task<void> JoinSlashCommand::route(const dpp::slashcommand_t &event)
{
    auto guildId = event.command.guild_id;
    auto guild = dpp::find_guild(guildId);
    if (not guild)
    {
        helpers::sendErrorEmbed(
            "Internal error: cannot find guild's data. Try again, and if "
            "this "
            "error persists contact bot's developer.",
            event);
        event.from->log(
            dpp::ll_error,
            std::format("Could not find guild in cache: {}", guildId));
        co_return;
    }
    auto voiceChannelForUser = guild->voice_members.find(event.command.usr.id);
    if (guild->voice_members.end() == voiceChannelForUser)
    {
        co_return helpers::sendErrorEmbed("You are not in a voice channel.",
                                          event);
    }
    auto channelId = voiceChannelForUser->second.channel_id;
    auto player = lavalinkConnector->getPlayerController(guildId);
    if (player->isConnected())
    {
        if (player->getVoiceChannelId() == channelId)
        {
            co_return helpers::sendErrorEmbed(
                "You are in the same voice chat as bot.", event);
        }
        co_return helpers::sendErrorEmbed(
            "Bot is already connected to voice channel.", event);
    }

    auto playerUpdateAsync =
        helpers::awaitWhen(lavalinkConnector->onPlayerUpdate,
                           [guildId](const lavalink::PlayerUpdateEvent &event)
                           { return event.guildId == guildId; });
    event.from->log(
        dpp::ll_debug,
        std::format("Connecting to voice chat with id '{}', guild id '{}'",
                    channelId, guildId));

    dpp::async thinking = event.co_thinking();
    co_await player->connect(channelId);
    co_await playerUpdateAsync;
    co_await thinking;
    event.edit_original_response(
        dpp::message(std::format("Connected to voice chat {}",
                                 dpp::utility::channel_mention(channelId))));
    event.from->log(
        dpp::ll_debug,
        "Finished invoking join command, bot connected to voice chat.");

    co_return;
}

dpp::task<void> LeaveSlashCommand::route(const dpp::slashcommand_t &event)
{
    const auto player = verifyIfCanUseCommand(event);
    if (not player)
    {
        co_return;
    }

    dpp::async thinking = event.co_thinking();
    const auto channelId = player->getVoiceChannelId();
    co_await player->disconnect();
    co_await thinking;
    event.edit_original_response(dpp::message(fmt::format(
        "Leaving voice chat {}", dpp::utility::channel_mention(channelId))));
    co_return;
}

dpp::slashcommand TestSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{TestSlashCommand::name},
                             "Test command", bot.me.id);
}

dpp::task<void> TestSlashCommand::route(const dpp::slashcommand_t &event)
{
    event.from->log(dpp::ll_debug, "Test command invoked.");
    auto aev = event.co_reply("Test command invoked.");
    auto guildId = event.command.guild_id;
    dpp::snowflake voiceChannelId = 1044643577222934559;
    auto player = lavalinkConnector->getPlayerController(guildId);
    co_await player->connect(voiceChannelId);
    if (player->isConnected())
    {
        event.from->log(dpp::ll_debug, "Player connected.");
    }
    else
    {
        event.from->log(dpp::ll_debug, "Player not connected.");
    }
    event.from->log(dpp::ll_debug, "Test command executed.");
    co_await aev;
    co_return event.edit_original_response(
        dpp::message("Test command executed."));
}
}  // namespace musicCommand
