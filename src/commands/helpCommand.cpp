#include "commands/helpCommand.hpp"

#include <dpp/dpp.h>

#include "commands/diceParser.hpp"
#include "config.hpp"
#include "helpers.hpp"

namespace helpCommand
{
void mainHelpPageContext(dpp::embed &embed)
{
    embed.set_title("Help");
    embed.set_description(
        "Below you can see available modules. Choose one for which you need to "
        "see help page");
    embed.add_field(":game_die: Roll",
                    "Roll dices of your choice, duel your friend (or foe), or "
                    "test your fate with coin or colored dice.",
                    false);
}

void rollHelpPageContext(dpp::embed &embed)
{
    auto parserLimits = diceParser::getLimits();
    std::string prefix = config::get("prefix");
    embed.set_title("Help for roll module");
    embed.set_description(
        "Roll dices of your choice, duel your friend (or foe), or test your "
        "fate with coin or colored dice.\n After every command you can add "
        "comment after !");

    embed.add_field(":game_die: " + prefix + "roll <repeats> <dice command>",
                    "Roll specified dice, add modifier and show total result "
                    "(rounded down for division)\nRepetitions are optional, "
                    "structure of dice command is shown below.",
                    false);

    embed.add_field(
        ":game_die: " + prefix + "roll <repeats> <color>",
        "Roll colored dice, and get result based on it's color. Each dice have "
        "different distribution of result, which can be Big Fail, Fail, Small "
        "fail, Small Success, Success or Big Success."
        "\n Repeats are optional. To see possible results and distribution for "
        "each dice, see 'colors' help page",
        false);

    embed.add_field(
        ":game_die: " + prefix + "roll <repeats> fate",
        "Roll coin and see, if fate is on your side\n Repeats are optional",
        false);

    embed.add_field(
        ":game_die: " + prefix +
            "roll duel <1st player dice command> <2nd player dice command> "
            "<1st player name> <2nd player name>",
        "Roll coin and see, if fate is on your side\n Roll count is optional",
        false);

    embed.add_field(":game_die: Dice command structure",
                    "**<dice count>d<dice size><modifier>**\n"
                    "**Dice count** is optional value which specifies how many "
                    "dice should be rolled at once, allowed range is between " +
                        std::to_string(parserLimits[0]) + "-" +
                        std::to_string(parserLimits[1]) +
                        "\n"
                        "**Dice size** is what it says, size of the dice "
                        "rolled, allowed range is between " +
                        std::to_string(parserLimits[2]) + "-" +
                        std::to_string(parserLimits[3]) +
                        "\n"
                        "**Dice modifier** is optional value which defines, "
                        "how sum of rolled dice should be modified. Allowed "
                        "operations are addition, subtraction, multiplication "
                        "and division, and maximum modifier value is " +
                        std::to_string(parserLimits[4]) +
                        "\n"
                        "Example dice commands: d10, 2d12, 8d6+4, 2d6*149856",
                    false);
}

void colorHelpPageContext(dpp::embed &embed)
{
    embed.set_title("Distribution of colored dice results");
    embed.set_description(
        "Below is shown the result distribution for each color of dice");
    embed.add_field(
        "Black", "Big Fail     - Big Fail      - Big Fail      - Small Success",
        false);
    embed.add_field(
        "Red", "Big Fail     - Big Fail      - Fail          - Success", false);
    embed.add_field("Orange",
                    "Big Fail     - Fail          - Fail          - Success",
                    false);
    embed.add_field(
        "White", "Big Fail     - Fail          - Success       - Big Success",
        false);
    embed.add_field(
        "Yellow", "Fail         - Success       - Success       - Big Success",
        false);
    embed.add_field(
        "Lime", "Fail         - Success       - Big Success   - Big Success",
        false);
    embed.add_field(
        "Green", "Small Fail   - Big Success   - Big Success   - Big Success",
        false);
}

template <typename Event>
void fillAndSendHelpPage(const Event &event, const std::string &command)
{
    dpp::embed helpEmbed{};
    helpEmbed.set_color(dpp::utility::rgb(0, 0, 255));
    if ("roll" == command)
    {
        rollHelpPageContext(helpEmbed);
    }
    else if ("color" == command)
    {
        colorHelpPageContext(helpEmbed);
    }
    else
    {
        mainHelpPageContext(helpEmbed);
    }

    event.reply(dpp::message{}.add_embed(helpEmbed));
}

void HelpMsgCommand::route(const dpp::message_create_t &event,
                           const std::string &command)
{
    fillAndSendHelpPage(event, command);
}

dpp::slashcommand HelpSlashCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{HelpSlashCommand::name},
                             "Show help page", bot.me.id)
        .set_dm_permission(true)
        .add_option(
            dpp::command_option(dpp::co_string, "page", "Help page to be shown")
                .add_choice(
                    dpp::command_option_choice{"Roll help page", "roll"})
                .add_choice(
                    dpp::command_option_choice{"Color distribution", "color"}));
}

dpp::task<void> HelpSlashCommand::route(const dpp::slashcommand_t &event)
{
    auto helpPage =
        helpers::getOptionalParamFromEvent<std::string>(event, "page", "");
    fillAndSendHelpPage(event, helpPage);
    co_return;
}
}  // namespace helpCommand
