#include "commands/rollCommand.hpp"

#include <bits/chrono.h>
#include <bits/types/time_t.h>
#include <ctype.h>
#include <dpp/dpp.h>
#include <fmt/format.h>
#include <limits.h>

#include <algorithm>
#include <array>
#include <cctype>
#include <chrono>
#include <cstdint>
#include <random>
#include <sstream>
#include <string>
#include <unordered_map>

#include "commands/diceParser.hpp"
#include "config.hpp"
#include "helpers.hpp"

namespace rollCommand
{
static thread_local std::mt19937 rngEngine;
static thread_local bool seeded{false};
constexpr int MAX_REPETITIONS = 24;
constexpr int MAX_EMBED_SIZE = 6000;
constexpr int ADDITIONAL_DESCRIPTION_SIZE = 43;
constexpr int COMMENT_MAX_SIZE = 100;
static const std::unordered_map<std::string, std::array<std::string, 4>>
    colors = {
        {"green", {"Small Fail", "Big Success", "Big Success", "Big Success"}},
        {"lime", {"Fail", "Success", "Big Success", "Big Success"}},
        {"yellow", {"Fail", "Success", "Success", "Big Success"}},
        {"white", {"Big Fail", "Fail", "Success", "Big Success"}},
        {"orange", {"Big Fail", "Fail", "Fail", "Success"}},
        {"red", {"Big Fail", "Big Fail", "Fail", "Success"}},
        {"black", {"Big Fail", "Big Fail", "Big Fail", "Small Success"}}};
static const std::unordered_map<std::string, std::uint32_t> embedColors = {
    {"green", dpp::utility::rgb(0, 128, 0)},
    {"lime", dpp::utility::rgb(50, 205, 50)},
    {"yellow", dpp::utility::rgb(255, 255, 0)},
    {"white", dpp::utility::rgb(255, 255, 255)},
    {"orange", dpp::utility::rgb(255, 104, 31)},
    {"red", dpp::utility::rgb(255, 0, 0)},
    {"black", dpp::utility::rgb(0, 0, 0)}};

template <typename Event>
std::string getNickFromEvent(const Event&)
{
    return "";
}

template <>
std::string getNickFromEvent(const dpp::message_create_t& event)
{
    return event.msg.author.username;
}

template <>
std::string getNickFromEvent(const dpp::slashcommand_t& event)
{
    return event.command.usr.username;
}

template <typename Event>
dpp::embed prepareResultEmbed(const Event& event, const std::string& comment)
{
    dpp::embed resultEmbed{};
    resultEmbed.set_title("Roll for " + getNickFromEvent(event))
        .set_description(
            comment.size() <= COMMENT_MAX_SIZE
                ? comment
                : (comment.substr(0, COMMENT_MAX_SIZE - 3) + "..."))
        .set_color(dpp::utility::rgb(0, 0, 255));
    return resultEmbed;
}

std::uint32_t roll(std::uint32_t maxNum)
{
    if (not seeded)
    {
        rngEngine.seed(
            std::chrono::system_clock::now().time_since_epoch().count());
        seeded = true;
    }
    std::uniform_int_distribution<std::uint32_t> dist(1, maxNum);
    return dist(rngEngine);
}

std::pair<std::string, std::int64_t> rollDicesAndStringify(
    diceParser::Dice dice)
{
    std::stringstream rollList("");
    std::int64_t sum{0};
    for (int i = 0; i < dice.count; i++)
    {
        auto randomNumber = roll(dice.size);
        sum += randomNumber;
        if (i > 0)
        {
            rollList << ", ";
        }
        rollList << randomNumber;
    }
    switch (dice.modifierType)
    {
        case '+':
            sum += dice.modifier;
            break;
        case '-':
            sum -= dice.modifier;
            break;
        case '*':
            sum *= dice.modifier;
            break;
        case '/':
            sum /= dice.modifier;
            break;
    }
    rollList << "\n**Result: " << std::fixed << sum << "**";
    return {rollList.str(), sum};
}

template <typename Event>
void duelCommand(const std::string& p1Command, const std::string& p2Command,
                 const std::string& p1Name, const std::string& p2Name,
                 const std::string& comment, const Event& event)
{
    auto resultEmbed = prepareResultEmbed(event, comment);
    resultEmbed.set_title("Duel fight: " + p1Name + " vs " + p2Name);
    auto p1ParseResult = diceParser::parseDice(p1Command);
    if (not p1ParseResult.wasSuccesfull())
    {
        auto error = diceParser::getErrorMessage(p1ParseResult.errorCause);
        error += "\nParsed command: '" + p1Command + "'";
        return helpers::sendErrorEmbed(error, event);
    }
    auto p2ParseResult = diceParser::parseDice(p2Command);
    if (not p2ParseResult.wasSuccesfull())
    {
        auto error = diceParser::getErrorMessage(p2ParseResult.errorCause);
        error += "\nParsed command: '" + p2Command + "'";
        return helpers::sendErrorEmbed(error, event);
    }
    auto& p1Dice = p1ParseResult.dice;
    auto& p2Dice = p2ParseResult.dice;

    auto p1Result = rollDicesAndStringify(p1Dice);
    auto p2Result = rollDicesAndStringify(p2Dice);
    if (p1Result.second > p2Result.second)
    {
        resultEmbed.add_field(p1Name + " won",
                              "__" + std::to_string(p1Result.second) +
                                  "__ vs " + std::to_string(p2Result.second),
                              false);
    }
    else if (p1Result.second < p2Result.second)
    {
        resultEmbed.add_field(p2Name + " won",
                              std::to_string(p1Result.second) + " vs __" +
                                  std::to_string(p2Result.second) + "__",
                              false);
    }
    else
    {
        resultEmbed.add_field("Draw",
                              std::to_string(p1Result.second) + " vs " +
                                  std::to_string(p2Result.second),
                              false);
    }

    resultEmbed.add_field(p1Name + "'s rolls", p1Result.first, false);
    resultEmbed.add_field(p2Name + "'s rolls", p2Result.first, false);
    event.reply(dpp::message{}.add_embed(resultEmbed));
}

template <typename Event>
void colorCommand(int repeats, std::string color, const std::string& comment,
                  const Event& event)
{
    auto resultEmbed = prepareResultEmbed(event, comment);
    resultEmbed.set_color(embedColors.at(color));
    for (int r = 1; r <= repeats; r++)
    {
        std::string colorResult = colors.at(color).at(roll(4) - 1);
        resultEmbed.add_field("Roll #" + std::to_string(r), colorResult, false);
    }
    event.reply(dpp::message{}.add_embed(resultEmbed));
}

template <typename Event>
void fateCommand(int repeats, const std::string& comment, const Event& event)
{
    auto resultEmbed = prepareResultEmbed(event, comment);
    for (int r = 1; r <= repeats; r++)
    {
        std::string fateResult = (roll(2) == 2)
                                     ? "Fate is on your side :thumbsup:"
                                     : "Fate is not on your side :thumbsdown:";
        resultEmbed.add_field("Roll #" + std::to_string(r), fateResult, false);
    }
    event.reply(dpp::message{}.add_embed(resultEmbed));
}

template <typename Event>
void rollCommand(int repeats, diceParser::Dice dice, const std::string& comment,
                 const Event& event)
{
    event.from->log(
        dpp::ll_debug,
        fmt::format(
            "Dice data received: count: {}, size: {}, modifier: {}, type: {}",
            dice.count, dice.size, dice.modifier, dice.modifierType));
    auto resultEmbed = prepareResultEmbed(event, comment);
    for (int r = 1; r <= repeats; r++)
    {
        resultEmbed.add_field("Roll #" + std::to_string(r),
                              rollDicesAndStringify(dice).first, true);
    }
    int totalEmbedSize =
        resultEmbed.title.size() + resultEmbed.description.size();
    auto& fields = resultEmbed.fields;
    std::size_t f;
    for (f = 0; f < fields.size(); f++)
    {
        auto& field = fields.at(0);
        totalEmbedSize += field.name.size() + field.value.size();
        if (totalEmbedSize > MAX_EMBED_SIZE - ADDITIONAL_DESCRIPTION_SIZE)
        {
            break;
        }
    }
    if (f < fields.size())
    {
        fields.erase(fields.begin() + f, fields.end());
        resultEmbed.description +=
            fmt::format("\n\n Limiting repeats to {} due to size limit", f);
    }
    event.reply(dpp::message{}.add_embed(resultEmbed));
}

template <typename Event>
void rollCommand(int repeats, const std::string& command,
                 const std::string& comment, const Event& event)
{
    auto parseResult = diceParser::parseDice(command);
    if (not parseResult.wasSuccesfull())
    {
        auto error = diceParser::getErrorMessage(parseResult.errorCause);
        error += "\nParsed command: '" + command + "'";
        return helpers::sendErrorEmbed(error, event);
    }
    rollCommand(repeats, parseResult.dice, comment, event);
}

void RollMsgCommand::route(const dpp::message_create_t& event,
                           const std::string& commandInfo)
{
    auto [command, comment] = helpers::splitOnFirstOf(commandInfo, "!");
    auto tokenizedCommand = helpers::tokenize(command);
    if (tokenizedCommand.empty())
    {
        // If no more command info was sent (apart from comment) treat as
        // default command
        event.from->log(dpp::ll_debug, "Received empty roll, treating as d10");
        rollCommand(1, diceParser::Dice{}, comment, event);
        return;
    }
    if ("duel" == tokenizedCommand[0])
    {
        std::string p1Command =
            tokenizedCommand.size() > 1 ? tokenizedCommand[1] : "d10";
        std::string p2Command =
            tokenizedCommand.size() > 2 ? tokenizedCommand[2] : "d10";
        std::string p1Name =
            tokenizedCommand.size() > 3 ? tokenizedCommand[3] : "Player 1";
        std::string p2Name =
            tokenizedCommand.size() > 4 ? tokenizedCommand[4] : "Player 2";
        return duelCommand(p1Command, p2Command, p1Name, p2Name, comment,
                           event);
    }
    std::string diceCommand{tokenizedCommand.at(0)};
    int repeatCount{1};
    if (tokenizedCommand.size() >= 2)
    {
        try
        {
            if (std::any_of(diceCommand.begin(), diceCommand.end(),
                            [](unsigned char c)
                            { return not std::isdigit(c); }))
            {
                throw std::invalid_argument{""};
            }
            int temp = stoi(tokenizedCommand.at(0));
            if (temp < 1 or MAX_REPETITIONS < temp)
            {
                throw std::out_of_range{""};
            }
            repeatCount = temp;
            diceCommand = tokenizedCommand.at(1);
        }
        catch (std::out_of_range&)
        {
            return helpers::sendErrorEmbed(
                fmt::format("Number of repetitions should be between {}-{}", 1,
                            MAX_REPETITIONS),
                event);
        }
        catch (std::invalid_argument&)
        {
            diceCommand = tokenizedCommand.at(0);
        }
    }
    event.from->log(
        dpp::ll_debug,
        fmt::format("Received roll with '{}' dice command, repeated {} times",
                    diceCommand, repeatCount));
    if ("fate" == diceCommand)
    {
        fateCommand(repeatCount, comment, event);
    }
    else if (("fail" == diceCommand) or ("failure" == diceCommand))
    {
        std::string resources = config::get("resourcesFolder");
        event.reply(dpp::message{}.add_file(
            "failure.jpg",
            dpp::utility::read_file(resources + "/failure.jpg")));
    }
    else if (colors.contains(diceCommand))
    {
        colorCommand(repeatCount, diceCommand, comment, event);
    }
    else
    {
        rollCommand(repeatCount, diceCommand, comment, event);
    }
}

dpp::task<void> RollSlashCommand::route(const dpp::slashcommand_t& event)
{
    auto comment =
        helpers::getOptionalParamFromEvent<std::string>(event, "comment", "");
    auto repeats =
        helpers::getOptionalParamFromEvent<long>(event, "repeats", 1);
    if (helpers::checkOptionalParamInEvent<std::string>(event, "dice_command"))
    {
        auto diceCommand = helpers::getOptionalParamFromEvent<std::string>(
            event, "dice_command", "");
        if (("fail" == diceCommand) or ("failure" == diceCommand))
        {
            std::string resources = config::get("resourcesFolder");
            event.reply(dpp::message{}.add_file(
                "failure.jpg",
                dpp::utility::read_file(resources + "/failure.jpg")));
            co_return;
        }
        rollCommand(repeats, diceCommand, comment, event);
        co_return;
    }
    auto diceModifier =
        helpers::getOptionalParamFromEvent<std::string>(event, "modifier", "");
    char modType{'\0'};
    std::uint32_t mod{};
    if (diceModifier.size() > 1)
    {
        if (0 != diceModifier.find_first_of("+-*/") or
            not std::all_of(diceModifier.begin() + 1, diceModifier.end(),
                            [](char c) { return std::isdigit(c); }))
        {
            co_return helpers::sendErrorEmbed("Dice modifier is not valid",
                                              event);
        }
        modType = diceModifier[0];
        mod = std::stoul(diceModifier.substr(1));
        if (mod > diceParser::getLimits()[4])
        {
            co_return helpers::sendErrorEmbed("Dice modifier is not valid",
                                              event);
        }
    }
    auto diceCount =
        helpers::getOptionalParamFromEvent<long>(event, "dice_count", 1);
    auto diceSize =
        helpers::getOptionalParamFromEvent<long>(event, "dice_size", 10);
    diceParser::Dice dice{
        static_cast<uint8_t>(diceCount),
        static_cast<uint32_t>(diceSize),
        ('\0' == modType) ? 0 : mod,
        ('\0' == modType) ? '+' : modType,
    };
    rollCommand(repeats, dice, comment, event);
}

dpp::task<void> ColorSlashCommand::route(const dpp::slashcommand_t& event)
{
    auto comment =
        helpers::getOptionalParamFromEvent<std::string>(event, "comment", "");
    auto repeatCount =
        helpers::getOptionalParamFromEvent<long>(event, "repeats", 1);
    auto color = helpers::getParamFromEvent<std::string>(event, "dice_color");
    event.from->log(dpp::ll_debug,
                    "Invoked color slash command with color: " + color);
    colorCommand(repeatCount, color, comment, event);
    co_return;
}

dpp::task<void> FateSlashCommand::route(const dpp::slashcommand_t& event)
{
    auto comment =
        helpers::getOptionalParamFromEvent<std::string>(event, "comment", "");
    auto repeatCount =
        helpers::getOptionalParamFromEvent<long>(event, "repeats", 1);
    fateCommand(repeatCount, comment, event);
    co_return;
}

dpp::task<void> DuelSlashCommand::route(const dpp::slashcommand_t& event)
{
    auto comment =
        helpers::getOptionalParamFromEvent<std::string>(event, "comment", "");
    auto p1Command = helpers::getOptionalParamFromEvent<std::string>(
        event, "p1_dice", "d10");
    auto p2Command = helpers::getOptionalParamFromEvent<std::string>(
        event, "p2_dice", "d10");
    auto p1Name = helpers::getOptionalParamFromEvent<std::string>(
        event, "p1_name", "Player 1");
    auto p2Name = helpers::getOptionalParamFromEvent<std::string>(
        event, "p2_name", "Player 2");
    duelCommand(p1Command, p2Command, p1Name, p2Name, comment, event);
    co_return;
}

dpp::slashcommand RollSlashCommand::registerCommand(dpp::cluster& bot)
{
    auto range = diceParser::getLimits();
    return dpp::slashcommand(std::string{RollSlashCommand::name}, "Roll a dice",
                             bot.me.id)
        .set_dm_permission(true)
        .add_option(dpp::command_option(dpp::co_integer, "repeats",
                                        "How many times should repeat rolls")
                        .set_min_value(1)
                        .set_max_value(24))
        .add_option(dpp::command_option(
                        dpp::co_integer, "dice_count",
                        "How many dice should be rolled at once, default 1")
                        .set_min_value(range[0])
                        .set_max_value(range[1]))
        .add_option(dpp::command_option(dpp::co_integer, "dice_size",
                                        "How many sides dices have, default 10")
                        .set_min_value(range[2])
                        .set_max_value(range[3]))
        .add_option(
            dpp::command_option(dpp::co_string, "modifier",
                                "Modifier applied to sum of dices, for example "
                                "+5, *3, /2, -10, max modifier value is " +
                                    std::to_string(range[4]))
                .set_max_length(10))
        .add_option(
            dpp::command_option(dpp::co_string, "dice_command",
                                "Full dice command, i.e. 5d4+3. Prioritized "
                                "over separate dice parameters")
                .set_max_length(35))
        .add_option(dpp::command_option(dpp::co_string, "comment",
                                        "Comment for this command")
                        .set_max_length(100));
}

dpp::slashcommand ColorSlashCommand::registerCommand(dpp::cluster& bot)
{
    return dpp::slashcommand(std::string{ColorSlashCommand::name},
                             "Roll a colored dice", bot.me.id)
        .set_dm_permission(true)
        .add_option(
            dpp::command_option(dpp::co_string, "dice_color",
                                "color of the dice", true)
                .add_choice(dpp::command_option_choice("green", "green"))
                .add_choice(dpp::command_option_choice("lime", "lime"))
                .add_choice(dpp::command_option_choice("yellow", "yellow"))
                .add_choice(dpp::command_option_choice("white", "white"))
                .add_choice(dpp::command_option_choice("orange", "orange"))
                .add_choice(dpp::command_option_choice("red", "red"))
                .add_choice(dpp::command_option_choice("black", "black")))
        .add_option(dpp::command_option(dpp::co_integer, "repeats",
                                        "How many times should repeat rolls")
                        .set_min_value(1)
                        .set_max_value(24))
        .add_option(dpp::command_option(dpp::co_string, "comment",
                                        "Comment for this command")
                        .set_max_length(100));
}

dpp::slashcommand FateSlashCommand::registerCommand(dpp::cluster& bot)
{
    return dpp::slashcommand(std::string{FateSlashCommand::name},
                             "Check your fate", bot.me.id)
        .set_dm_permission(true)
        .add_option(dpp::command_option(dpp::co_integer, "repeats",
                                        "How many times should repeat rolls")
                        .set_min_value(1)
                        .set_max_value(24))
        .add_option(dpp::command_option(dpp::co_string, "comment",
                                        "Comment for this command")
                        .set_max_length(100));
}

dpp::slashcommand DuelSlashCommand::registerCommand(dpp::cluster& bot)
{
    return dpp::slashcommand(std::string{DuelSlashCommand::name}, "Roll a dice",
                             bot.me.id)
        .set_dm_permission(true)
        .add_option(dpp::command_option(
                        dpp::co_string, "p1_dice",
                        "Full dice command for first player, i.e. 5d4+3")
                        .set_max_length(35))
        .add_option(dpp::command_option(
                        dpp::co_string, "p2_dice",
                        "Full dice command for second player, i.e. 5d4+3")
                        .set_max_length(35))
        .add_option(dpp::command_option(dpp::co_string, "p1_name",
                                        "First player's name")
                        .set_max_length(35))
        .add_option(dpp::command_option(dpp::co_string, "p2_name",
                                        "Second player's name")
                        .set_max_length(35))
        .add_option(dpp::command_option(dpp::co_string, "comment",
                                        "Comment for this command")
                        .set_max_length(100));
}
}  // namespace rollCommand
