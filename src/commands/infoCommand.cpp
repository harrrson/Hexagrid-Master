#include "commands/infoCommand.hpp"

#include <dpp/dpp.h>

namespace infoCommand
{
dpp::slashcommand InfoCommand::registerCommand(dpp::cluster &bot)
{
    return dpp::slashcommand(std::string{InfoCommand::name}, "Show current bot",
                             bot.me.id)
        .set_dm_permission(true);
}

dpp::task<void> InfoCommand::route(const dpp::slashcommand_t &event)
{
    dpp::embed statusEmbed{};
    statusEmbed.set_title("Hexagrid Master")
        .add_field("Bot uptime", event.from->creator->uptime().to_string());

    event.reply(
        dpp::message{}.add_embed(statusEmbed).set_flags(dpp::m_ephemeral));
    co_return;
}
}  // namespace infoCommand
