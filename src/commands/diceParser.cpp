#include "commands/diceParser.hpp"

#include <fmt/format.h>

namespace diceParser
{
std::uint8_t MIN_DICE_COUNT = 1;
std::uint8_t MAX_DICE_COUNT = 40;
std::uint32_t MIN_DICE_SIZE = 2;
std::uint32_t MAX_DICE_SIZE = UINT32_MAX / 10;
std::uint32_t MAX_DICE_MOD = UINT32_MAX / 10;

enum class ParserStage : int
{
    DICE_COUNT = 0,
    DICE_SIZE,
    DICE_MOD,
    parseEnd
};

ParseResult parseDice(const std::string& dice)
{
    Dice result{};
    std::string number{};
    ErrorCause errorCause{ErrorCause::NO_ERROR};
    ParserStage stage = ParserStage::DICE_COUNT;
    auto extractNumberWithLimits = [&errorCause, &number](
                                       std::uint32_t min, std::uint32_t max,
                                       ErrorCause error) -> long long
    {
        long long extractedNum{};
        try
        {
            extractedNum = stoll(number);
        }
        catch (std::out_of_range&)
        {
            errorCause = error;
            return 0;
        }
        catch (std::invalid_argument&)
        {
            errorCause = ErrorCause::IMPROPER_COMMAND_FORMAT;
            return 0;
        }
        number.clear();
        if (extractedNum < min or max < extractedNum)
        {
            errorCause = error;
        }
        return extractedNum;
    };
    for (const auto& c : dice)
    {
        if (ErrorCause::NO_ERROR != errorCause)
        {
            break;
        }
        switch (c)
        {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                number.push_back(c);
                break;
            case 'd':
            case 'D':
                if (ParserStage::DICE_COUNT != stage)
                {
                    errorCause = ErrorCause::IMPROPER_COMMAND_FORMAT;
                    break;
                }
                if (not number.empty())
                {
                    result.count = extractNumberWithLimits(
                        MIN_DICE_COUNT, MAX_DICE_COUNT,
                        ErrorCause::DICE_COUNT_OUT_OF_RANGE);
                }
                stage = ParserStage::DICE_SIZE;
                break;
            case '+':
            case '-':
            case '*':
            case '/':
                if (ParserStage::DICE_SIZE != stage)
                {
                    errorCause = ErrorCause::IMPROPER_COMMAND_FORMAT;
                    break;
                }
                result.modifierType = c;
                result.size =
                    extractNumberWithLimits(MIN_DICE_SIZE, MAX_DICE_SIZE,
                                            ErrorCause::DICE_SIZE_OUT_OF_RANGE);
                stage = ParserStage::DICE_MOD;
                break;
            default:
                errorCause = ErrorCause::IMPROPER_COMMAND_FORMAT;
                break;
        }
    }
    if (ErrorCause::NO_ERROR == errorCause)
    {
        switch (stage)
        {
            case ParserStage::DICE_SIZE:
                result.size =
                    extractNumberWithLimits(MIN_DICE_SIZE, MAX_DICE_SIZE,
                                            ErrorCause::DICE_SIZE_OUT_OF_RANGE);
                break;
            case ParserStage::DICE_MOD:
                result.modifier = extractNumberWithLimits(
                    0, MAX_DICE_MOD, ErrorCause::DICE_MOD_OUT_OF_RANGE);
                break;
            default:
                break;
        }
        if (result.modifierType == '/' and (0 == result.modifier))
        {
            errorCause = ErrorCause::DIV_BY_0;
        }
    }
    return {result, errorCause};
}

std::string getErrorMessage(ErrorCause cause)
{
    switch (cause)
    {
        case ErrorCause::DICE_COUNT_OUT_OF_RANGE:
            return fmt::format(
                "Dice command parse error: Dice count (value before D) is out "
                "of range {}-{}",
                MIN_DICE_COUNT, MAX_DICE_COUNT);
        case ErrorCause::DICE_SIZE_OUT_OF_RANGE:
            return fmt::format(
                "Dice command parse error: Dice size (value after D) is out of "
                "range {}-{}",
                MIN_DICE_SIZE, MAX_DICE_SIZE);
        case ErrorCause::DICE_MOD_OUT_OF_RANGE:
            return fmt::format(
                "Dice command parse error: Dice modifier value can be maximum "
                "{}",
                MAX_DICE_MOD);
        case ErrorCause::DIV_BY_0:
            return "Dice command parse error: You cannot divide by 0";
        case ErrorCause::IMPROPER_COMMAND_FORMAT:
            return "Dice command parse error: Dice command format was not "
                   "valid, use <dice count>d<dice size><modifier>";
        default:
            return "Uh oh, you should not be able to see this message. Contact "
                   "bot developer with exact command you used";
    }
}

void setLimits(int minDice, int maxDice, int minSize, int maxSize, int maxMod)
{
    MIN_DICE_COUNT = minDice;
    MAX_DICE_COUNT = maxDice;
    MIN_DICE_SIZE = minSize;
    MAX_DICE_SIZE = maxSize;
    MAX_DICE_MOD = maxMod;
}

std::array<std::uint32_t, 5> getLimits()
{
    return {MIN_DICE_COUNT, MAX_DICE_COUNT, MIN_DICE_SIZE, MAX_DICE_SIZE,
            MAX_DICE_MOD};
}
}  // namespace diceParser
