#include "commands/downloader/downloader.hpp"

#include <curl/curl.h>
#include <dpp/misc-enum.h>
#include <dpp/utility.h>
#include <fmt/core.h>
#include <fmt/format.h>

#include <condition_variable>
#include <cstdio>
#include <deque>
#include <filesystem>
#include <mutex>
#include <thread>

#include "commands/downloader/transcode.hpp"
// #include "commands/downloader/transcoder/transcoder.hpp"

namespace commands::downloader
{
namespace
{
using namespace commands::downloader;

typedef std::function<void(const DownloadResult&)> DownloadCallback;

struct DownloadRequest
{
    std::string downloadLink{};
    std::string filePath{};
    std::string fileName{};
    DownloadCallback callback{};
    FileType fileType{FileType::generic};
};

std::mutex queueMutex;
std::deque<DownloadRequest> requestQueue{};
std::condition_variable workCV{};
std::atomic<bool> alive{true};
std::atomic<bool> alreadyInitiated{false};
std::thread workerThread;
dpp::cluster* bot;
const std::string tmpDownloadPath{"/tmp/Hexagrid/download"};

struct ReencodingData
{
    enum class InAudioFormat
    {
        unknown,
        mp3,
        opus
    } audioFormat{InAudioFormat::unknown};
    std::stringstream inData{};
    std::stringstream rawData{};
    FILE* targetFile{nullptr};
    bool decodeToOpus{false};
};

void removeTmpFile(const std::filesystem::path& path)
{
    if (std::filesystem::exists(path))
    {
        std::filesystem::remove(path);
    }
}

void downloadGenericFile(const DownloadRequest& queueElem)
{
    CURL* handle = curl_easy_init();
    if (not handle)
    {
        queueElem.callback({false});
        return;
    }
    FILE* fd{nullptr};
    std::filesystem::path tempFile{"/nonexistent"};
    bot->log(dpp::ll_info,
             fmt::format("File type: {}", queueElem.fileType == FileType::audio
                                              ? "audio"
                                              : "generic"));
    // if (queueElem.fileType == FileType::audio)
    // {
    //     bot->log(dpp::ll_info, "Parsing audio file.");
    //     std::filesystem::path tempFolder{tmpDownloadPath};
    //     if (not std::filesystem::exists(tempFolder))
    //     {
    //         std::filesystem::create_directories(tempFolder);
    //     }
    //     tempFile = tempFolder / queueElem.fileName;
    // }
    // else
    // {
    tempFile = queueElem.filePath;
    // }
    fd = fopen(tempFile.c_str(), "wb");
    if (not fd)
    {
        bot->log(dpp::ll_info, "Cannot open download file");
        curl_easy_cleanup(handle);
        queueElem.callback({false});
        return;
    }
    curl_easy_setopt(handle, CURLOPT_URL, queueElem.downloadLink.c_str());
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, fd);
    CURLcode res = curl_easy_perform(handle);

    curl_easy_cleanup(handle);
    fclose(fd);
    if (res != CURLE_OK)
    {
        bot->log(dpp::ll_info, "Error during file download");
        queueElem.callback({false});
        removeTmpFile(tempFile);
        return;
    }
    // if (queueElem.fileType == FileType::audio)
    // {
    //     bot->log(dpp::ll_info, "Transcoding audio file.");
    //     // auto result = transcodeToOpus(tempFile, queueElem.filePath);
    //     // if (result != Result::success)
    //     transcoder::OpusTranscoder transcoder{tempFile, queueElem.filePath,
    //                                           bot};
    //     auto result = transcoder.transcode();
    //     if (result != transcoder::OpusTranscoder::Result::success)
    //     {
    //         removeTmpFile(tempFile);
    //         queueElem.callback({false});
    //     }
    // }
    removeTmpFile(tempFile);
    queueElem.callback({true});
}

// void downloadAudioFile(const DownloadRequest& req)
// {
//     std::stringstream cmd{};
//     cmd <<"/usr/bin/ffmpeg -hide_banner -n -i
//     "<<std::quoted(req.downloadLink)<<" -c libopus -ar 48000 -vn
//     "<<std::quoted(req.filePath) <<" 2>&1";
//         bot->log(dpp::ll_trace, fmt::format("Command: {}", cmd.str()));
//     FILE* downloader = popen(cmd.str().c_str(), "r");
//     if (not downloader)
//     {
//         req.callback({false});
//         return;
//     }
//     #define BUFSIZE 256
//     char buf[BUFSIZE];
//     while (fgets(buf, BUFSIZE, downloader))
//     {
//         bot->log(dpp::ll_trace, fmt::format("Downloader trace chunk: {}",
//         buf));
//     }
//     int status = pclose(downloader);
//     bot->log(dpp::ll_trace, fmt::format("Downloader status: {}", status));
//     if(status == -1)
//     {
//         req.callback({false});
//         return;
//     }
//     req.callback({true});

// }

void enqueueRequest(const std::string& url, const std::string& filePath,
                    const std::string& fileName, FileType filetype,
                    DownloadCallback cb)
{
    std::unique_lock<std::mutex> lk(queueMutex);
    requestQueue.emplace_back(
        DownloadRequest{url, filePath, fileName, cb, filetype});
    workCV.notify_one();
}

void worker()
{
    while (alive)
    {
        if (requestQueue.empty())
        {
            std::unique_lock<std::mutex> cvlock(queueMutex);
            workCV.wait(cvlock, [&]()
                        { return not alive or not requestQueue.empty(); });
        }
        if (not alive)
        {
            break;
        }
        auto getQueueElement = [&]()
        {
            std::unique_lock<std::mutex> lk(queueMutex);
            auto elem = requestQueue.front();
            requestQueue.pop_front();
            return elem;
        };
        auto queueElem = getQueueElement();
        // switch(queueElem.fileType)
        // {
        // case FileType::generic:
        downloadGenericFile(queueElem);
        // case FileType::audio:
        //     downloadAudioFile(queueElem);
        // }
    }
}
}  // namespace

void init(dpp::cluster* bot_)
{
    if (alreadyInitiated)
    {
        return;
    }

    workerThread = std::thread{&worker};
    alreadyInitiated = true;
    bot = bot_;
    initTranscoder(bot);
}

void shutdown()
{
    alive = false;
    workCV.notify_all();
    workerThread.join();
    for (auto request : requestQueue)
    {
        request.callback({false});
    }
}

dpp::async<DownloadResult> downloadFile(const std::string& url,
                                        const std::string& filePath,
                                        const std::string& fileName,
                                        FileType fileType)
{
    return dpp::async<DownloadResult>{
        static_cast<void (*)(const std::string&, const std::string&,
                             const std::string&, FileType, DownloadCallback)>(
            &enqueueRequest),
        url, filePath, fileName, fileType};
}
}  // namespace commands::downloader
