#include "commands/downloader/transcode.hpp"

#include <dpp/dpp.h>
#include <dpp/misc-enum.h>
#include <fmt/format.h>

#include <string>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavcodec/codec.h>
#include <libavcodec/packet.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/avutil.h>
#include <libavutil/channel_layout.h>
#include <libavutil/frame.h>
#include <libavutil/mathematics.h>
#include <libavutil/mem.h>
#include <libavutil/opt.h>
#include <libavutil/samplefmt.h>
}

#include <utility>
#include <vector>

namespace commands::downloader
{
// using SharedAVFormatContext = std::shared_ptr<AVFormatContext>;

namespace

{
dpp::cluster *bot;

class InputAVFormatContext
{
    AVFormatContext *ctxt{nullptr};

   public:
    InputAVFormatContext() = default;

    ~InputAVFormatContext()
    {
        avformat_close_input(&ctxt);
    }

    AVFormatContext *get()
    {
        return ctxt;
    }

    AVFormatContext **getPtrAddr()
    {
        return &ctxt;
    }

    AVFormatContext *operator->()
    {
        return ctxt;
    }
};

class OutputAVFormatContext
{
    AVFormatContext *ctxt{nullptr};

   public:
    OutputAVFormatContext() = default;

    ~OutputAVFormatContext()
    {
        avformat_close_input(&ctxt);
    }

    AVFormatContext *get()
    {
        return ctxt;
    }

    AVFormatContext **getPtrAddr()
    {
        return &ctxt;
    }

    AVFormatContext *operator->()
    {
        return ctxt;
    }
};

struct StreamContext
{
    AVCodecContext *decCtx{nullptr};
    AVCodecContext *encCtx{nullptr};

    AVFrame *decFrame{nullptr};
    StreamContext() = default;
    StreamContext(StreamContext &) = delete;

    StreamContext(StreamContext &&other)
        : decCtx(std::exchange(other.decCtx, nullptr)),
          encCtx(std::exchange(other.encCtx, nullptr)),
          decFrame(std::exchange(other.decFrame, nullptr))
    {
    }

    StreamContext &operator=(StreamContext &) = delete;

    StreamContext &operator=(StreamContext &&other)
    {
        std::swap(decCtx, other.decCtx);
        std::swap(encCtx, other.encCtx);
        std::swap(decFrame, other.decFrame);
        return *this;
    }

    ~StreamContext()
    {
        if (decCtx)
        {
            avcodec_free_context(&decCtx);
        }
        if (encCtx)
        {
            avcodec_free_context(&encCtx);
        }
        if (decFrame)
        {
            av_frame_free(&decFrame);
        }
    }
};

class SmartCodecContext
{
    AVCodecContext *ctx{nullptr};
    SmartCodecContext() = delete;

   public:
    SmartCodecContext(const AVCodec *codec) : ctx(avcodec_alloc_context3(codec))
    {
    }

    ~SmartCodecContext()
    {
        if (ctx)
        {
            avcodec_free_context(&ctx);
        }
    }

    AVCodecContext *get()
    {
        return ctx;
    };

    AVCodecContext *operator->()
    {
        return ctx;
    };

    void passOwnership(AVCodecContext **target)
    {
        *target = ctx;
        ctx = nullptr;
    }
};

struct FilteringContext
{
    AVFilterContext *buffersinkCtx{nullptr};
    AVFilterContext *buffersrcCtx{nullptr};
    AVFilterGraph *filterGraph{nullptr};

    AVPacket *encPkt{nullptr};
    AVFrame *filteredFrame{nullptr};

    FilteringContext() = default;
    FilteringContext(FilteringContext &) = delete;

    FilteringContext(FilteringContext &&other)
        : buffersinkCtx(std::exchange(other.buffersinkCtx, nullptr)),
          buffersrcCtx(std::exchange(other.buffersrcCtx, nullptr)),
          filterGraph(std::exchange(other.filterGraph, nullptr)),
          encPkt(std::exchange(other.encPkt, nullptr)),
          filteredFrame(std::exchange(other.filteredFrame, nullptr))
    {
    }

    FilteringContext &operator=(FilteringContext &) = delete;

    FilteringContext &operator=(FilteringContext &&other)
    {
        std::swap(buffersinkCtx, other.buffersinkCtx);
        std::swap(buffersrcCtx, other.buffersrcCtx);
        std::swap(filterGraph, other.filterGraph);
        std::swap(encPkt, other.encPkt);
        std::swap(filteredFrame, other.filteredFrame);
        return *this;
    }

    ~FilteringContext()
    {
        if (buffersinkCtx)
        {
            avfilter_free(buffersinkCtx);
        }
        if (buffersrcCtx)
        {
            avfilter_free(buffersrcCtx);
        }
        if (filterGraph)
        {
            avfilter_graph_free(&filterGraph);
        }
        if (encPkt)
        {
            av_packet_free(&encPkt);
        }
        if (filteredFrame)
        {
            av_frame_free(&filteredFrame);
        }
    }
};

struct AVInOuts
{
    AVFilterInOut *outputs{nullptr};
    AVFilterInOut *inputs{nullptr};

    AVInOuts() : outputs(avfilter_inout_alloc()), inputs(avfilter_inout_alloc())
    {
    }

    ~AVInOuts()
    {
        if (outputs)
        {
            avfilter_inout_free(&outputs);
        }
        if (inputs)
        {
            avfilter_inout_free(&inputs);
        }
    }
};

class SmartPacket
{
    AVPacket *packet{nullptr};

   public:
    SmartPacket() : packet(av_packet_alloc()) {}

    ~SmartPacket()
    {
        if (packet)
        {
            av_packet_free(&packet);
        }
    };

    AVPacket *get()
    {
        return packet;
    }

    AVPacket *operator->()
    {
        return packet;
    }
};

using Streams = std::vector<StreamContext>;

using Filters = std::vector<FilteringContext>;

// TODO: Add logging to formatter
int openInput(const std::string &inFile, InputAVFormatContext &iCtx,
              Streams &streams)
{
    int ret{0};
    if ((ret = avformat_open_input(iCtx.getPtrAddr(), inFile.c_str(), nullptr,
                                   nullptr)) < 0)
    {
        bot->log(dpp::ll_info, "Error with opening input file.");
        return ret;
    }
    if ((ret = avformat_find_stream_info(iCtx.get(), nullptr)))
    {
        bot->log(dpp::ll_info, "Error with getting stream info.");
        return ret;
    }

    for (unsigned int i = 0; i < iCtx->nb_streams; i++)
    {
        auto *stream = iCtx->streams[i];
        const AVCodec *dec = avcodec_find_decoder(stream->codecpar->codec_id);
        if (not dec)
        {
            bot->log(dpp::ll_info, "Error: decoder not found.");
            return AVERROR_DECODER_NOT_FOUND;
        }

        streams.emplace_back();
        auto *decoder = avcodec_alloc_context3(dec);
        if (not decoder)
        {
            bot->log(dpp::ll_info, "Error with allogating decoder.");
            return AVERROR(ENOMEM);
        }
        streams[i].decCtx = decoder;
        if ((ret = avcodec_parameters_to_context(decoder, stream->codecpar)) <
            0)
        {
            bot->log(dpp::ll_info,
                     "Error with settings params to decoder context.");
            return ret;
        }

        if (decoder->codec_type != AVMEDIA_TYPE_AUDIO)
        {
            bot->log(dpp::ll_info, "Currently can be used only with audio.");
            return AVERROR_UNKNOWN;
        }
        decoder->pkt_timebase = stream->time_base;
        ret = avcodec_open2(decoder, dec, nullptr);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Error in opening decoder.");
            return ret;
        }

        streams[i].decFrame = av_frame_alloc();
        if (not streams[i].decFrame)
        {
            return AVERROR(ENOMEM);
        }
    }
    return 0;
}

int openOutput(const std::string &outFile, OutputAVFormatContext &oCtx,
               Streams &streams)
{
    int ret{0};
    avformat_alloc_output_context2(oCtx.getPtrAddr(), nullptr, nullptr,
                                   outFile.c_str());
    if (not oCtx.get())
    {
        bot->log(dpp::ll_info, "Error in initiating input context.");
        return AVERROR_UNKNOWN;
    }
    for (unsigned int i = 0; i < streams.size(); i++)
    {
        auto *outStream = avformat_new_stream(oCtx.get(), nullptr);
        if (not outStream)
        {
            bot->log(dpp::ll_info, "Error in creating new stream for output.");
            return AVERROR_UNKNOWN;
        }

        const auto *encoder = avcodec_find_encoder(AV_CODEC_ID_OPUS);
        if (not encoder)
        {
            bot->log(dpp::ll_info, "Error while getting encoder.");
            return AVERROR_INVALIDDATA;
        }
        auto *encCtx = avcodec_alloc_context3(encoder);
        if (not encCtx)
        {
            bot->log(dpp::ll_info, "Error while allocating encoder context.");
            return AVERROR(ENOMEM);
        }
        streams[i].encCtx = encCtx;

        encCtx->bit_rate = 96000;
        encCtx->sample_rate = 48000;
        encCtx->sample_fmt = encoder->sample_fmts[0];
        encCtx->time_base = (AVRational){1, encCtx->sample_rate};
        ret = av_channel_layout_from_string(&encCtx->ch_layout, "stereo");
        if (ret < 0)
        {
            bot->log(dpp::ll_info,
                     "Error while getting channel layout for encoder.");
            return ret;
        }
        if (oCtx->oformat->flags & AVFMT_GLOBALHEADER)
        {
            encCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        }

        bot->log(dpp::ll_info,
                 fmt::format("Sample rate: {}", encCtx->sample_rate));
        ret = avcodec_open2(encCtx, encoder, nullptr);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Error while opening encoder.");
            return ret;
        }
        ret = avcodec_parameters_from_context(outStream->codecpar, encCtx);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Error while setting output stream params.");
            return ret;
        }
        outStream->time_base = encCtx->time_base;
    }
    if (!(oCtx->oformat->flags & AVFMT_NOFILE))
    {
        ret = avio_open(&(oCtx->pb), outFile.c_str(), AVIO_FLAG_WRITE);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Error while opening output file.");
            return ret;
        }
    }
    ret = avformat_write_header(oCtx.get(), nullptr);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Error while writing header to output file.");
        return ret;
    }
    return 0;
}

int initFilter(FilteringContext &fCtx, StreamContext &stream,
               const std::string &filterSpec)
{
    auto &encCtx = stream.encCtx;
    auto &decCtx = stream.decCtx;
    int ret{0};
    fCtx.filterGraph = avfilter_graph_alloc();
    AVInOuts inOuts{};
    if (not fCtx.filterGraph or not inOuts.inputs or not inOuts.outputs)
    {
        bot->log(dpp::ll_info,
                 "Error during filter's initial params allocation.");
        return AVERROR(ENOMEM);
    }
    const auto *buffersrc = avfilter_get_by_name("abuffer");
    const auto *buffersink = avfilter_get_by_name("abuffersink");
    if (not buffersrc or not buffersink)
    {
        bot->log(dpp::ll_info, "Cannot find filtering source or sink element.");
        return ret;
    }
    if (decCtx->ch_layout.order == AV_CHANNEL_ORDER_UNSPEC)
    {
        av_channel_layout_default(&(decCtx->ch_layout),
                                  decCtx->ch_layout.nb_channels);
    }
    constexpr int bufSize = 64;
    constexpr int argsSize = 512;
    std::vector<char> buf(bufSize);
    std::vector<char> args(argsSize);
    av_channel_layout_describe(&(decCtx->ch_layout), &buf[0], buf.size());
    snprintf(&args[0], args.size(),
             "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=%s",
             decCtx->pkt_timebase.num, decCtx->pkt_timebase.den,
             decCtx->sample_rate, av_get_sample_fmt_name(decCtx->sample_fmt),
             &buf[0]);
    ret = avfilter_graph_create_filter(&(fCtx.buffersrcCtx), buffersrc, "in",
                                       &args[0], nullptr, fCtx.filterGraph);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Cannot create audio buffer source.");
        return ret;
    }

    ret = avfilter_graph_create_filter(&(fCtx.buffersinkCtx), buffersink, "out",
                                       nullptr, nullptr, fCtx.filterGraph);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Cannot create audio buffer sink.");
        return ret;
    }

    ret = av_opt_set_bin(fCtx.buffersinkCtx, "sample_fmts",
                         (uint8_t *)&encCtx->sample_fmt,
                         sizeof(encCtx->sample_fmt), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Cannot set output sample format.");
        return ret;
    }

    av_channel_layout_describe(&encCtx->ch_layout, &buf[0], buf.size());
    ret = av_opt_set(fCtx.buffersinkCtx, "ch_layouts", &buf[0],
                     AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Cannot set output channel layout.");
        return ret;
    }

    ret = av_opt_set_bin(fCtx.buffersinkCtx, "sample_rates",
                         (uint8_t *)&encCtx->sample_rate,
                         sizeof(encCtx->sample_rate), AV_OPT_SEARCH_CHILDREN);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Cannot set output sample rate.");
        return ret;
    }

    inOuts.outputs->name = av_strdup("in");
    inOuts.outputs->filter_ctx = fCtx.buffersrcCtx;
    inOuts.outputs->pad_idx = 0;
    inOuts.outputs->next = nullptr;

    inOuts.inputs->name = av_strdup("out");
    inOuts.inputs->filter_ctx = fCtx.buffersinkCtx;
    inOuts.inputs->pad_idx = 0;
    inOuts.inputs->next = nullptr;
    if (not inOuts.outputs->name or not inOuts.inputs->name)
    {
        bot->log(dpp::ll_info, "Cannot set output/input name.");
        return AVERROR(ENOMEM);
    }

    ret =
        avfilter_graph_parse_ptr(fCtx.filterGraph, filterSpec.c_str(),
                                 &(inOuts.inputs), &(inOuts.outputs), nullptr);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Error while parsing filter graph.");
        return ret;
    }

    ret = avfilter_graph_config(fCtx.filterGraph, nullptr);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Error while configuring filter graph.");
        return ret;
    }
    return 0;
}

int initFilters(Filters &filters, Streams &streams)
{
    int ret{0};
    for (size_t i = 0; i < filters.size(); i++)
    {
        auto &filter = filters[i];
        auto &stream = streams[i];
        ret = initFilter(filter, stream, "anull");
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Error during filter creation.");
            return ret;
        }
        filter.encPkt = av_packet_alloc();
        if (not filter.encPkt)
        {
            bot->log(dpp::ll_info, "Error during filter packet allocation.");
            return ret;
        }
        filter.filteredFrame = av_frame_alloc();
        if (not filter.filteredFrame)
        {
            bot->log(dpp::ll_info, "Error during filtered frame allocation.");
            return ret;
        }
    }
    return 0;
}

int encode_write_frame(unsigned int streamId, int flush,
                       FilteringContext &filter, StreamContext &stream,
                       OutputAVFormatContext &oCtx)
{
    AVFrame *filtFrame = (flush ? nullptr : filter.filteredFrame);
    AVPacket *encPkt = filter.encPkt;
    int ret{};
    av_packet_unref(encPkt);
    if (filtFrame and (filtFrame->pts != AV_NOPTS_VALUE))
    {
        filtFrame->pts = av_rescale_q(filtFrame->pts, filtFrame->time_base,
                                      stream.encCtx->time_base);
    }

    ret = avcodec_send_frame(stream.encCtx, filtFrame);

    if (ret < 0)
    {
        return ret;
    }

    while (ret >= 0)
    {
        ret = avcodec_receive_packet(stream.encCtx, encPkt);
        if ((ret == AVERROR(EAGAIN) or (ret == AVERROR_EOF)))
        {
            return 0;
        }
        encPkt->stream_index = streamId;

        av_packet_rescale_ts(encPkt, stream.encCtx->time_base,
                             oCtx->streams[streamId]->time_base);

        ret = av_interleaved_write_frame(oCtx.get(), encPkt);
    }
    return ret;
}

int filter_encode_write_frame(AVFrame *frame, unsigned int streamId,
                              FilteringContext &filter, StreamContext &stream,
                              OutputAVFormatContext &oCtx)
{
    int ret{};

    ret = av_buffersrc_add_frame_flags(filter.buffersrcCtx, frame, 0);
    if (ret < 0)
    {
        bot->log(dpp::ll_info, "Error while feeding the filter graph");
        return ret;
    }

    while (1)
    {
        ret =
            av_buffersink_get_frame(filter.buffersinkCtx, filter.filteredFrame);

        if (ret < 0)
        {
            if ((ret == AVERROR(EAGAIN)) or (ret == AVERROR_EOF))
            {
                ret = 0;
            }
            break;
        }
        filter.filteredFrame->time_base =
            av_buffersink_get_time_base(filter.buffersinkCtx);
        filter.filteredFrame->pict_type = AV_PICTURE_TYPE_NONE;
        ret = encode_write_frame(streamId, 0, filter, stream, oCtx);
        av_frame_unref(filter.filteredFrame);
        if (ret < 0)
        {
            break;
        }
    }
    return ret;
}

static int flush_encoder(unsigned int stream_index, StreamContext &stream,
                         FilteringContext &filter, OutputAVFormatContext &oCtx)
{
    if (!(stream.encCtx->codec->capabilities & AV_CODEC_CAP_DELAY))
    {
        return 0;
    }

    av_log(NULL, AV_LOG_INFO, "Flushing stream #%u encoder\n", stream_index);
    return encode_write_frame(stream_index, 1, filter, stream, oCtx);
}
}  // namespace

void initTranscoder(dpp::cluster *bot_)
{
    bot = bot_;
}

Result transcodeToOpus(const std::string &inFile, const std::string &outFile)
{
    InputAVFormatContext iAvFormat{};
    Streams streams{};
    int ret;
    if ((ret = openInput(inFile, iAvFormat, streams)) < 0)
    {
        bot->log(dpp::ll_info, "Error in opening input file.");
        return Result::fail;
    }
    OutputAVFormatContext oAvFormat{};
    if ((ret = openOutput(outFile, oAvFormat, streams)) < 0)
    {
        bot->log(dpp::ll_info, "Error in opening output file.");
        return Result::fail;
    }
    bot->log(dpp::ll_info, "Succesfull file parsing.");
    Filters filters(streams.size());
    if ((ret = initFilters(filters, streams)) < 0)
    {
        bot->log(dpp::ll_info, "Error while initializing filters.");
        return Result::fail;
    }
    SmartPacket packet{};
    if (not packet.get())
    {
        return Result::fail;
    }

    unsigned int streamId{};
    // Processing loop
    while (1)
    {
        ret = av_read_frame(iAvFormat.get(), packet.get());
        if (ret < 0)
        {
            break;
        }
        streamId = packet->stream_index;

        auto &stream = streams[streamId];
        // TODO: Do some sort of packet buffer, because
        ret = avcodec_send_packet(stream.decCtx, packet.get());
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Decoding failed");
            break;
        }
        bool error{ret < 0};
        while (ret >= 0)
        {
            ret = avcodec_receive_frame(stream.decCtx, stream.decFrame);
            if (ret < 0)
            {
                error = not((ret == AVERROR_EOF) or (ret == AVERROR(EAGAIN)));
                break;
            }
            stream.decFrame->pts = stream.decFrame->best_effort_timestamp;
            ret =
                filter_encode_write_frame(stream.decFrame, streamId,
                                          filters[streamId], stream, oAvFormat);
            if (ret < 0)
            {
                break;
            }
        }
        if (ret < 0 and error)
        {
            bot->log(dpp::ll_info, "Encoding failed");
            break;
        }

        av_packet_unref(packet.get());
    }
    if (ret < 0)
    {
        return Result::fail;
    }

    for (size_t i = 0; i < iAvFormat->nb_streams; i++)
    {
        if (not(filters[i].filterGraph))
        {
            continue;
        }
        auto &stream = streams[i];

        ret = avcodec_send_packet(stream.decCtx, nullptr);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "flushing decoding context failed");
            break;
        }

        while (ret >= 0)
        {
            ret = avcodec_receive_frame(stream.decCtx, stream.decFrame);
            if (ret == AVERROR_EOF)
            {
                break;
            }
            else if (ret < 0)
            {
                break;
            }

            stream.decFrame->pts = stream.decFrame->best_effort_timestamp;
            ret = filter_encode_write_frame(
                stream.decFrame, i, filters[streamId], stream, oAvFormat);
            if (ret < 0)
            {
                break;
            }
        }

        /* flush filter */
        ret = filter_encode_write_frame(NULL, i, filters[streamId], stream,
                                        oAvFormat);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Flushing filter failed\n");
            break;
        }

        /* flush encoder */
        ret = flush_encoder(i, stream, filters[i], oAvFormat);
        if (ret < 0)
        {
            bot->log(dpp::ll_info, "Flushing encoder failed\n");
            break;
        }
    }
    av_write_trailer(oAvFormat.get());

    if (ret < 0)
    {
        return Result::fail;
    }
    return Result::success;
}
}  // namespace commands::downloader
