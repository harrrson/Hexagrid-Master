#include <curl/curl.h>
#include <dpp/dpp.h>

#include <atomic>
#include <chrono>
#include <csignal>
#include <stdexcept>
#include <thread>

#include "commands/downloader/downloader.hpp"
#include "commands/musicCommand.hpp"
#include "config.hpp"
#include "listeners.hpp"
#include "logger.hpp"

std::atomic<bool> running{true};

void stopBot(int)
{
    running = false;
}

int main(int argc, char* argv[])
{
    if (argc <= 1)
    {
        throw(std::runtime_error{"You need to pass path to config file!"});
    }
    config::initConfig(argv[1]);
    logger::initLogger();
    curl_global_init(CURL_GLOBAL_DEFAULT);

    auto intents = dpp::intents::i_default_intents |
                   dpp::intents::i_guild_members |
                   dpp::intents::i_message_content;

    dpp::cluster bot(config::get("token"), intents);
    commands::downloader::init(&bot);

    bot.on_log(&logger::log);
    bot.on_ready(&listeners::on_ready);
    bot.on_message_create(&listeners::on_message);
    bot.on_slashcommand(&listeners::on_slashcommand);

    bot.start(dpp::st_return);

    signal(SIGINT, &stopBot);

    while (running)
    {
        std::this_thread::sleep_for(std::chrono::seconds{1});
    }

    bot.log(dpp::ll_info, "SIGINT detected, stopping bot");
    commands::downloader::shutdown();
    // TODO: Gracefully shutdown player threads
    // TODO: Gracefully stop bot

    curl_global_cleanup();
    return 0;
}
