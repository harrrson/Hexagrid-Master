#include "lavalink/types.hpp"

#include <nlohmann/json_fwd.hpp>

namespace lavalink
{

void from_json(const nlohmann::json& j, InfoResponse& info)
{
    j.at("version").get_to(info.version);
    j.at("buildTime").get_to(info.buildTime);
    j.at("git").get_to(info.git);
    j.at("jvm").get_to(info.jvm);
    j.at("lavaplayer").get_to(info.lavaplayer);
    j.at("sourceManagers").get_to(info.sourceManagers);
    j.at("filters").get_to(info.filters);
    j.at("plugins").get_to(info.plugins);
}

void from_json(const nlohmann::json& j, Version& info)
{
    j.at("semver").get_to(info.semver);
    j.at("major").get_to(info.major);
    j.at("minor").get_to(info.minor);
    j.at("patch").get_to(info.patch);
    if (j.contains("preRelease") and not j.at("preRelease").is_null())
    {
        j.at("preRelease").get_to(info.preRelease);
    }
    if (j.contains("build") and not j.at("build").is_null())
    {
        j.at("build").get_to(info.build);
    }
}

void from_json(const nlohmann::json& j, Git& info)
{
    j.at("branch").get_to(info.branch);
    j.at("commit").get_to(info.commit);
    j.at("commitTime").get_to(info.commitTime);
}

void from_json(const nlohmann::json& j, Plugin& info)
{
    j.at("name").get_to(info.name);
    j.at("version").get_to(info.version);
}

void from_json(const nlohmann::json& j, Memory& info)
{
    j.at("free").get_to(info.free);
    j.at("used").get_to(info.used);
    j.at("allocated").get_to(info.allocated);
    j.at("reservable").get_to(info.reservable);
}

void from_json(const nlohmann::json& j, CPU& info)
{
    j.at("cores").get_to(info.cores);
    j.at("systemLoad").get_to(info.systemLoad);
    j.at("lavalinkLoad").get_to(info.lavalinkLoad);
}

void from_json(const nlohmann::json& j, FrameStats& info)
{
    j.at("sent").get_to(info.sent);
    j.at("nulled").get_to(info.nulled);
    j.at("deficit").get_to(info.deficit);
}

void from_json(const nlohmann::json& j, Stats& info)
{
    j.at("players").get_to(info.players);
    j.at("playingPlayers").get_to(info.playingPlayers);
    j.at("uptime").get_to(info.uptime);
    j.at("memory").get_to(info.memory);
    j.at("cpu").get_to(info.cpu);
    if (j.contains("frameStats"))
    {
        j.at("frameStats").get_to(info.frameStats);
    }
}

void from_json(const nlohmann::json& j, PlayerState& info)
{
    j.at("time").get_to(info.time);
    j.at("position").get_to(info.position);
    j.at("connected").get_to(info.connected);
    j.at("ping").get_to(info.ping);
}

void from_json(const nlohmann::json& j, Track& info)
{
    j.at("encoded").get_to(info.encoded);
    j.at("info").get_to(info.info);
}

void from_json(const nlohmann::json& j, TrackInfo& info)
{
    j.at("identifier").get_to(info.identifier);
    j.at("isSeekable").get_to(info.isSeekable);
    j.at("author").get_to(info.author);
    j.at("length").get_to(info.length);
    j.at("isStream").get_to(info.isStream);
    j.at("position").get_to(info.position);
    j.at("title").get_to(info.title);
    from_json(j.at("uri"), info.uri);
    from_json(j.at("artworkUrl"), info.artworkUrl);
    from_json(j.at("isrc"), info.isrc);
    j.at("sourceName").get_to(info.sourceName);
}

void from_json(const nlohmann::json& j, TrackEndReason& info)
{
    if (j == "finished")
    {
        info = TrackEndReason::finished;
    }
    else if (j == "loadFailed")
    {
        info = TrackEndReason::loadFailed;
    }
    else if (j == "stopped")
    {
        info = TrackEndReason::stopped;
    }
    else if (j == "replaced")
    {
        info = TrackEndReason::replaced;
    }
    else if (j == "cleanup")
    {
        info = TrackEndReason::cleanup;
    }
}

void from_json(const nlohmann::json& j, Exception& info)
{
    from_json(j.at("message"), info.message);
    j.at("severity").get_to(info.severity);
    j.at("cause").get_to(info.cause);
}

void from_json(const nlohmann::json& j, Severity& info)
{
    if (j == "common")
    {
        info = Severity::common;
    }
    else if (j == "suspicious")
    {
        info = Severity::suspicious;
    }
    else if (j == "fault")
    {
        info = Severity::fault;
    }
}

void from_json(const nlohmann::json& j, Player& info)
{
    j.at("guildId").get_to(info.guildId);
    if (j.contains("track"))
    {
        j.at("track").get_to(info.track);
    }
    j.at("volume").get_to(info.volume);
    j.at("paused").get_to(info.paused);
    j.at("state").get_to(info.state);
    j.at("voice").get_to(info.voice);
    j.at("filters").get_to(info.filters);
}

void from_json(const nlohmann::json& j, Filters& info)
{
    if (j.contains("volume"))
    {
        from_json(j.at("volume"), info.volume);
    }

    j.at("equalizer").get_to(info.equalizer);

    if (j.contains("karaoke"))
    {
        j.at("karaoke").get_to(info.karaoke);
    }
    if (j.contains("timescale"))
    {
        j.at("timescale").get_to(info.timescale);
    }
    if (j.contains("tremolo"))
    {
        j.at("tremolo").get_to(info.tremolo);
    }
    if (j.contains("vibrato"))
    {
        j.at("vibrato").get_to(info.vibrato);
    }
    if (j.contains("rotation"))
    {
        j.at("rotation").get_to(info.rotation);
    }
    if (j.contains("distortion"))
    {
        j.at("distortion").get_to(info.distortion);
    }
    if (j.contains("channelMix"))
    {
        j.at("channelMix").get_to(info.channelMix);
    }
    if (j.contains("lowPass"))
    {
        j.at("lowPass").get_to(info.lowPass);
    }
    if (j.contains("pluginFilters") and not j.at("pluginFilters").is_null())
    {
        info.pluginFilters =
            j.at("pluginFilters").get<std::map<std::string, PluginFilters>>();
    }
}

void from_json(const nlohmann::json& j, Equalizer& info)
{
    j.at("band").get_to(info.band);
    j.at("gain").get_to(info.gain);
}

void from_json(const nlohmann::json& j, VoiceState& info)
{
    j.at("token").get_to(info.token);
    j.at("endpoint").get_to(info.endpoint);
    j.at("sessionId").get_to(info.sessionId);
}

void to_json(nlohmann::json& j, const VoiceState& info)
{
    j = nlohmann::json{{"token", info.token},
                       {"endpoint", info.endpoint},
                       {"sessionId", info.sessionId}};
}

void from_json(const nlohmann::json& j, Karaoke& info)
{
    if (j.contains("level"))
    {
        from_json(j.at("level"), info.level);
    }
    if (j.contains("monoLevel"))
    {
        from_json(j.at("monoLevel"), info.monoLevel);
    }
    if (j.contains("filterBand"))
    {
        from_json(j.at("filterBand"), info.filterBand);
    }
    if (j.contains("filterWidth"))
    {
        from_json(j.at("filterWidth"), info.filterWidth);
    }
}

void from_json(const nlohmann::json& j, Timescale& info)
{
    if (j.contains("speed"))
    {
        from_json(j.at("speed"), info.speed);
    }
    if (j.contains("pitch"))
    {
        from_json(j.at("pitch"), info.pitch);
    }
    if (j.contains("rate"))
    {
        from_json(j.at("rate"), info.rate);
    }
}

void from_json(const nlohmann::json& j, Tremolo& info)
{
    if (j.contains("frequency"))
    {
        from_json(j.at("frequency"), info.frequency);
    }
    if (j.contains("depth"))
    {
        from_json(j.at("depth"), info.depth);
    }
}

void from_json(const nlohmann::json& j, Vibrato& info)
{
    if (j.contains("frequency"))
    {
        from_json(j.at("frequency"), info.frequency);
    }
    if (j.contains("depth"))
    {
        from_json(j.at("depth"), info.depth);
    }
}

void from_json(const nlohmann::json& j, Rotation& info)
{
    if (j.contains("rotationHz"))
    {
        from_json(j.at("rotationHz"), info.rotationHz);
    }
}

void from_json(const nlohmann::json& j, Distortion& info)
{
    if (j.contains("sinOffset"))
    {
        from_json(j.at("sinOffset"), info.sinOffset);
    }
    if (j.contains("sinScale"))
    {
        from_json(j.at("sinScale"), info.sinScale);
    }
    if (j.contains("cosOffset"))
    {
        from_json(j.at("cosOffset"), info.cosOffset);
    }
    if (j.contains("cosScale"))
    {
        from_json(j.at("cosScale"), info.cosScale);
    }
    if (j.contains("tanOffset"))
    {
        from_json(j.at("tanOffset"), info.tanOffset);
    }
    if (j.contains("tanScale"))
    {
        from_json(j.at("tanScale"), info.tanScale);
    }
    if (j.contains("offset"))
    {
        from_json(j.at("offset"), info.offset);
    }
    if (j.contains("scale"))
    {
        from_json(j.at("scale"), info.scale);
    }
}

void from_json(const nlohmann::json& j, ChannelMix& info)
{
    if (j.contains("leftToLeft"))
    {
        from_json(j.at("leftToLeft"), info.leftToLeft);
    }
    if (j.contains("leftToRight"))
    {
        from_json(j.at("leftToRight"), info.leftToRight);
    }
    if (j.contains("rightToLeft"))
    {
        from_json(j.at("rightToLeft"), info.rightToLeft);
    }
    if (j.contains("rightToRight"))
    {
        from_json(j.at("rightToRight"), info.rightToRight);
    }
}

void from_json(const nlohmann::json& j, LowPass& info)
{
    if (j.contains("smoothing"))
    {
        from_json(j.at("smoothing"), info.smoothing);
    }
}

void to_json(nlohmann::json& j, const UpdatePlayerTrack& info)
{
    if (not info.encoded.empty())
    {
        j["encoded"] = info.encoded;
    }
    if (not info.identifier.empty())
    {
        j["info"] = info.identifier;
    }
}
}  // namespace lavalink
