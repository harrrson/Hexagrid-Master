#include "lavalink/LavalinkWebsocket.hpp"

#include <dpp/dpp.h>
#include <fmt/core.h>

#include <cstdint>
#include <ctime>

#include "lavalink/LavalinkConnector.hpp"

namespace lavalink
{
constexpr unsigned char WS_FINBIT = (1u << 7u);
constexpr unsigned char WS_PAYLOAD_LENGTH_MAGIC_LARGE = 126;
constexpr unsigned char WS_PAYLOAD_LENGTH_MAGIC_HUGE = 127;
constexpr size_t WS_MAX_PAYLOAD_LENGTH_SMALL = 125;
constexpr size_t WS_MAX_PAYLOAD_LENGTH_LARGE = 65535;
constexpr size_t MAXHEADERSIZE = sizeof(uint64_t) + 2;

LavalinkWebsocket::LavalinkWebsocket(const std::string &hostname,
                                     const std::string &port,
                                     const std::string &password,
                                     LavalinkConnector *owner_,
                                     dpp::cluster *bot_)
    : dpp::ssl_client(hostname, port, true),
      state(httpHeaders),
      path("/v4/websocket"),
      dataOpcode(OP_BINARY),
      password(password),
      owner(owner_),
      bot(bot_)
{
    uint64_t k = (time(nullptr) * time(nullptr));

    key = dpp::to_hex(k);
    key = dpp::base64_encode(
        reinterpret_cast<const unsigned char *>(key.c_str()), key.length());

    try
    {
        this->connect();
    }
    catch (const std::exception &e)
    {
        cleanup();
        throw;
    }

    bot->on_voice_server_update([&](const dpp::voice_server_update_t &) {});
    bot->on_voice_state_update([&](const dpp::voice_state_update_t &) {});
}

void LavalinkWebsocket::connect()
{
    state = httpHeaders;
    /* Send headers synchronously */
    uint64_t uid = bot->me.id;
    std::string clientName = "Hexagrid-Master/0.0.1";
    try
    {
        this->write("GET " + this->path +
                    " HTTP/1.1\r\n"
                    "Host: " +
                    this->hostname +
                    "\r\n"
                    "pragma: no-cache\r\n"
                    "User-Agent: " +
                    dpp::http_version +
                    "\r\n"
                    "Upgrade: WebSocket\r\n"
                    "Connection: Upgrade\r\n"
                    "User-Id: " +
                    std::to_string(uid) +
                    "\r\n"
                    "Client-Name:" +
                    clientName +
                    "\r\n"
                    "Authorization: " +
                    password +
                    "\r\n"
                    "Sec-WebSocket-Key: " +
                    this->key +
                    "\r\n"
                    "Sec-WebSocket-Version: 13\r\n\r\n");
    }
    catch (const std::exception &e)
    {
        bot->log(dpp::ll_error, "Exception caught: " + std::string(e.what()));
        throw;
    }
}

LavalinkWebsocket::~LavalinkWebsocket()
{
    cleanup();
}

void LavalinkWebsocket::run()
{
    runner = new std::thread(&LavalinkWebsocket::runLoop, this);
}

void LavalinkWebsocket::onReady(nlohmann::json &event)
{
    bot->log(dpp::ll_debug, "LavalinkWebsocket::onReady");
    bot->log(dpp::ll_trace, event.dump());
    sessionId = event["sessionId"].get<std::string>();
}

void LavalinkWebsocket::onPlayerUpdate(nlohmann::json &event)
{
    if (not this->owner->onPlayerUpdate.empty())
    {
        bot->log(dpp::ll_debug, event.dump());
        PlayerUpdateEvent e;
        e.bot = this->bot;
        e.connector = this->owner;
        std::string guildIdTmp;
        event["guildId"].get_to(guildIdTmp);
        e.guildId = guildIdTmp;
        event["state"].get_to(e.state);
        this->owner->onPlayerUpdate.call(e);
    }
}

void LavalinkWebsocket::onStats(nlohmann::json &event)
{
    if (not this->owner->onStats.empty())
    {
        StatsEvent e;
        e.bot = this->bot;
        e.connector = this->owner;
        event["stats"].get_to(e.stats);
        this->owner->onStats.call(e);
    }
}

void LavalinkWebsocket::onEvent(nlohmann::json &event)
{
    std::string type = event["type"].get<std::string>();
    if (type == "TrackStartEvent")
    {
        if (not this->owner->onTrackStart.empty())
        {
            TrackStartEvent e;
            e.bot = this->bot;
            e.connector = this->owner;
            std::string guildIdTmp;
            event["guildId"].get_to(guildIdTmp);
            e.guildId = guildIdTmp;
            event["track"].get_to(e.track);
            this->owner->onTrackStart.call(e);
        }
    }
    else if (type == "TrackEndEvent")
    {
        if (not this->owner->onTrackEnd.empty())
        {
            TrackEndEvent e;
            e.bot = this->bot;
            e.connector = this->owner;
            std::string guildIdTmp;
            event["guildId"].get_to(guildIdTmp);
            e.guildId = guildIdTmp;
            event["track"].get_to(e.track);
            event["reason"].get_to(e.reason);
            this->owner->onTrackEnd.call(e);
        }
    }
    else if (type == "TrackExceptionEvent")
    {
        if (not this->owner->onTrackException.empty())
        {
            TrackExceptionEvent e;
            e.bot = this->bot;
            e.connector = this->owner;
            std::string guildIdTmp;
            event["guildId"].get_to(guildIdTmp);
            e.guildId = guildIdTmp;
            event["track"].get_to(e.track);
            event["error"].get_to(e.error);
            this->owner->onTrackException.call(e);
        }
    }
    else if (type == "TrackStuckEvent")
    {
        if (not this->owner->onTrackStuck.empty())
        {
            TrackStuckEvent e;
            e.bot = this->bot;
            e.connector = this->owner;
            std::string guildIdTmp;
            event["guildId"].get_to(guildIdTmp);
            e.guildId = guildIdTmp;
            event["track"].get_to(e.track);
            event["threshold"].get_to(e.threshold);
            this->owner->onTrackStuck.call(e);
        }
    }
    else if (type == "WebSocketClosedEvent")
    {
        if (not this->owner->onWebSocketClosed.empty())
        {
            WebSocketClosedEvent e;
            e.bot = this->bot;
            e.connector = this->owner;
            std::string guildIdTmp;
            event["guildId"].get_to(guildIdTmp);
            e.guildId = guildIdTmp;
            event["code"].get_to(e.code);
            event["reason"].get_to(e.reason);
            event["byRemote"].get_to(e.byRemote);
            this->owner->onWebSocketClosed.call(e);
        }
    }
}

void LavalinkWebsocket::runLoop()
{
    dpp::utility::set_thread_name("LavalinkWebsocket");
    bot->log(dpp::ll_debug, "LavalinkWebsocket::runLoop");
    do
    {
        bool error = false;
        dpp::ssl_client::read_loop();
        if (!terminating)
        {
            bot->log(dpp::ll_info, "SSL read loop broken");
            dpp::ssl_client::close();
            do
            {
                bot->log(dpp::ll_info, "Reconnecting to Lavalink");
                error = false;
                try
                {
                    dpp::ssl_client::connect();
                    this->connect();
                }
                catch (const std::exception &e)
                {
                    bot->log(dpp::ll_info,
                             "Exception caught: " + std::string(e.what()));
                    dpp::ssl_client::close();
                    std::this_thread::sleep_for(std::chrono::seconds(5));
                    error = true;
                }
            } while (error);
        }
    } while (!terminating);
    if (this->sfd != INVALID_SOCKET)
    {
        this->nonblocking = false;
        this->send_close_packet();
        ssl_client::close();
    }
}

void LavalinkWebsocket::cleanup()
{
    terminating = true;
    if (runner)
    {
        runner->join();
        delete runner;
    }
}

bool LavalinkWebsocket::handle_frame(const std::string &buffer)
{
    nlohmann::json j;
    j = nlohmann::json::parse(buffer);
    auto o = j.find("op");
    if (o == j.end() || o->is_null())
    {
        return true;
    }
    auto op = o->get<std::string>();
    if (op == "event")
    {
        onEvent(j);
    }
    else if (op == "stats")
    {
        onStats(j);
    }
    else if (op == "playerUpdate")
    {
        onPlayerUpdate(j);
    }
    else if (op == "ready")
    {
        onReady(j);
    }
    return true;
}

void LavalinkWebsocket::send_close_packet()
{
    /* This is a 16 bit value representing 1000 in hex (0x03E8), network order.
     * For an error/close frame, this is all we need to send, just two bytes
     * and the header. We do this on shutdown of a websocket for graceful close.
     */
    std::string payload = "\x03\xE8";
    unsigned char out[MAXHEADERSIZE];

    size_t s = this->fill_header(out, payload.length(), OP_CLOSE);
    std::string header((const char *)out, s);
    ssl_client::write(header);
    ssl_client::write(payload);
}

size_t LavalinkWebsocket::fill_header(unsigned char *outbuf, size_t sendlength,
                                      ws_opcode opcode)
{
    size_t pos = 0;
    outbuf[pos++] = WS_FINBIT | opcode;

    if (sendlength <= WS_MAX_PAYLOAD_LENGTH_SMALL)
    {
        outbuf[pos++] = (unsigned int)sendlength;
    }
    else if (sendlength <= WS_MAX_PAYLOAD_LENGTH_LARGE)
    {
        outbuf[pos++] = WS_PAYLOAD_LENGTH_MAGIC_LARGE;
        outbuf[pos++] = (sendlength >> 8) & 0xff;
        outbuf[pos++] = sendlength & 0xff;
    }
    else
    {
        outbuf[pos++] = WS_PAYLOAD_LENGTH_MAGIC_HUGE;
        const uint64_t len = sendlength;
        for (int i = sizeof(uint64_t) - 1; i >= 0; i--)
        {
            outbuf[pos++] = ((len >> i * 8) & 0xff);
        }
    }

    return pos;
}

void LavalinkWebsocket::write(const std::string &data)
{
    if (state == httpHeaders)
    {
        /* Simple write */
        ssl_client::write(data);
    }
    else
    {
        unsigned char out[MAXHEADERSIZE];
        size_t s = this->fill_header(out, data.length(), this->dataOpcode);
        std::string header((const char *)out, s);
        ssl_client::write(header);
        ssl_client::write(data);
    }
}

bool LavalinkWebsocket::handle_buffer(std::string &buffer)
{
    switch (state)
    {
        case httpHeaders:
            if (buffer.find("\r\n\r\n") != std::string::npos)
            {
                /* Got all headers, proceed to new state */

                /* Get headers string */
                std::string headers = buffer.substr(0, buffer.find("\r\n\r\n"));

                /* Modify buffer, remove headers section */
                buffer.erase(0, buffer.find("\r\n\r\n") + 4);

                /* Process headers into map */
                std::vector<std::string> h = dpp::utility::tokenize(headers);
                if (h.size())
                {
                    std::string status_line = h[0];
                    h.erase(h.begin());
                    /* HTTP/1.1 101 Switching Protocols */
                    std::vector<std::string> status =
                        dpp::utility::tokenize(status_line, " ");
                    if (status.size() >= 3 && status[1] == "101")
                    {
                        for (auto &hd : h)
                        {
                            std::string::size_type sep = hd.find(": ");
                            if (sep != std::string::npos)
                            {
                                std::string key = hd.substr(0, sep);
                                std::string value =
                                    hd.substr(sep + 2, hd.length());
                                http_headers[key] = value;
                            }
                        }

                        state = connected;
                    }
                    else if (status.size() < 3)
                    {
                        log(dpp::ll_warning,
                            "Malformed HTTP response on websocket");
                        return false;
                    }
                    else if (status[1] != "200" && status[1] != "204")
                    {
                        log(dpp::ll_warning,
                            "Received unhandled code: " + status[1]);
                        return false;
                    }
                }
            }
            break;
        case connected:
            /* Process packets until we can't */
            while (this->parseheader(buffer))
                ;
            break;
    }
    return true;
}

void LavalinkWebsocket::log(dpp::loglevel severity,
                            const std::string &msg) const
{
    bot->log(severity, msg);
}

bool LavalinkWebsocket::parseheader(std::string &data)
{
    if (data.size() < 4)
    {
        /* Not enough data to form a frame yet */
        return false;
    }
    else
    {
        unsigned char opcode = data[0];
        switch (opcode & ~WS_FINBIT)
        {
            case OP_CONTINUATION:
            case OP_TEXT:
            case OP_BINARY:
            case OP_PING:
            case OP_PONG:
            {
                unsigned char len1 = data[1];
                unsigned int payloadstartoffset = 2;

                /* 6 bit ("small") length frame */
                uint64_t len = len1;

                if (len1 == WS_PAYLOAD_LENGTH_MAGIC_LARGE)
                {
                    /* 24 bit ("large") length frame */
                    if (data.length() < 8)
                    {
                        /* We don't have a complete header yet */
                        return false;
                    }

                    unsigned char len2 = (unsigned char)data[2];
                    unsigned char len3 = (unsigned char)data[3];
                    len = (len2 << 8) | len3;

                    payloadstartoffset += 2;
                }
                else if (len1 == WS_PAYLOAD_LENGTH_MAGIC_HUGE)
                {
                    /* 64 bit ("huge") length frame */
                    if (data.length() < 10)
                    {
                        /* We don't have a complete header yet */
                        return false;
                    }
                    len = 0;
                    for (int v = 2, shift = 56; v < 10; ++v, shift -= 8)
                    {
                        unsigned char l = (unsigned char)data[v];
                        len |= (uint64_t)(l & 0xff) << shift;
                    }
                    payloadstartoffset += 8;
                }

                if (data.length() < payloadstartoffset + len)
                {
                    /* We don't have a complete frame yet */
                    return false;
                }

                if ((opcode & ~WS_FINBIT) == OP_PING ||
                    (opcode & ~WS_FINBIT) == OP_PONG)
                {
                    handle_ping_pong((opcode & ~WS_FINBIT) == OP_PING,
                                     data.substr(payloadstartoffset, len));
                }
                else
                {
                    /* Pass this frame to the deriving class */
                    this->handle_frame(data.substr(payloadstartoffset, len));
                }

                /* Remove this frame from the input buffer */
                data.erase(data.begin(),
                           data.begin() + payloadstartoffset + len);

                return true;
            }
            break;

            case OP_CLOSE:
            {
                uint16_t error = data[2] & 0xff;
                error <<= 8;
                error |= (data[3] & 0xff);
                this->error(error);
                return false;
            }
            break;

            default:
            {
                this->error(0);
                return false;
            }
            break;
        }
    }
    return false;
}

void LavalinkWebsocket::one_second_timer()
{
    if (((time(nullptr) % 20) == 0) && (state == connected))
    {
        /* For sending pings, we send with payload */
        unsigned char out[MAXHEADERSIZE];
        std::string payload = "keepalive";
        size_t s = this->fill_header(out, payload.length(), OP_PING);
        std::string header((const char *)out, s);
        ssl_client::write(header);
        ssl_client::write(payload);
    }
}

void LavalinkWebsocket::handle_ping_pong(bool ping, const std::string &payload)
{
    if (ping)
    {
        /* For receiving pings we echo back their payload with the type OP_PONG
         */
        unsigned char out[MAXHEADERSIZE];
        size_t s = this->fill_header(out, payload.length(), OP_PONG);
        std::string header((const char *)out, s);
        ssl_client::write(header);
        ssl_client::write(payload);
    }
}

void LavalinkWebsocket::error(uint32_t) {}

void LavalinkWebsocket::close()
{
    this->state = httpHeaders;
    ssl_client::close();
}

std::string LavalinkWebsocket::getSessionId() const
{
    return sessionId;
}
}  // namespace lavalink
