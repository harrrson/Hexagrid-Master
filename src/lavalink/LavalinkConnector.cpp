#include "lavalink/LavalinkConnector.hpp"

#include <dpp/dpp.h>
#include <fmt/core.h>

#include <utility>

#include "lavalink/LavalinkWebsocket.hpp"
#include "lavalink/types.hpp"

namespace lavalink
{

LavalinkConnector::LavalinkConnector(dpp::cluster* bot_, const std::string& ip,
                                     std::uint32_t port,
                                     const std::string& password,
                                     const std::string& apiVerison)
    : host(ip),
      port(port),
      bot(bot_),
      authHeader({"Authorization", password}),
      apiVersion(apiVerison)
{
    hostUrl = "http://" + host + ":" + std::to_string(port);

    bot->on_voice_server_update(
        [this](const dpp::voice_server_update_t& event)
        {
            if (playerControllers.contains(event.guild_id))
            {
                playerControllers[event.guild_id]->onVoiceServerUpdate(event);
            }
        });
    this->onPlayerUpdate(
        [this](const PlayerUpdateEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onPlayerUpdate(event);
            }
        });
    this->onTrackStart(
        [this](const TrackStartEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onTrackStart(event);
            }
        });
    this->onTrackEnd(
        [this](const TrackEndEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onTrackEnd(event);
            }
        });
    this->onTrackException(
        [this](const TrackExceptionEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onTrackException(event);
            }
        });
    this->onTrackStuck(
        [this](const TrackStuckEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onTrackStuck(event);
            }
        });
    this->onWebSocketClosed(
        [this](const WebSocketClosedEvent& event)
        {
            if (playerControllers.contains(event.guildId))
            {
                playerControllers[event.guildId]->onWebSocketClosed(event);
            }
        });
}

void LavalinkConnector::run()
{
    ws = new LavalinkWebsocket(host, std::to_string(port), authHeader.second,
                               this, bot);
    ws->run();
}

dpp::task<ResponseCallback> LavalinkConnector::co_getLavalinkInfo()
{
    std::string endpoint = "/" + apiVersion + "/info";

    auto response = co_await requestGet(endpoint);

    if (response.status != 200)
    {
        co_return ResponseCallback{false, std::monostate{}};
    }

    InfoResponse info;
    nlohmann::json j = nlohmann::json::parse(response.body);
    bot->log(dpp::ll_trace, fmt::format("Response body: {}", j.dump()));
    from_json(j, info);
    co_return {true, info};
}

dpp::task<ResponseCallback> LavalinkConnector::co_getLavalinkVersion()
{
    std::string endpoint = "/version";

    auto response = co_await requestGet(endpoint);

    if (response.status != 200)
    {
        co_return ResponseCallback{false, std::monostate{}};
    }

    std::string version;
    bot->log(dpp::ll_trace, fmt::format("Response body: {}", response.body));
    co_return {true, version};
}

dpp::task<ResponseCallback> LavalinkConnector::co_getLavalinkStats()
{
    std::string endpoint = "/" + apiVersion + "/stats";

    auto response = co_await requestGet(endpoint);

    if (response.status != 200)
    {
        co_return ResponseCallback{false, std::monostate{}};
    }

    Stats stats;
    nlohmann::json j = nlohmann::json::parse(response.body);
    j.get_to(stats);
    co_return {true, stats};
}

dpp::task<ResponseCallback> LavalinkConnector::co_getPlayer(
    const dpp::snowflake& guildId)
{
    std::string endpoint = sessionEndpoint() + "/players/" + guildId.str();

    auto response = co_await requestGet(endpoint);

    if (response.status != 200)
    {
        co_return ResponseCallback{false, std::monostate{}};
    }
    Player p;
    nlohmann::json j = nlohmann::json::parse(response.body);
    j.get_to(p);

    co_return ResponseCallback{false, p};
}

dpp::task<ResponseCallback> LavalinkConnector::co_getPlayers()
{
    std::string endpoint = sessionEndpoint() + "/players";

    auto response = co_await requestGet(endpoint);

    if (response.status != 200)
    {
        co_return ResponseCallback{false, std::monostate{}};
    }
    PlayerList p;
    nlohmann::json j = nlohmann::json::parse(response.body);
    j.get_to(p);

    co_return ResponseCallback{false, p};
}

uint32_t LavalinkConnector::shardIdForGuild(const dpp::snowflake& guildId)
{
    return (guildId >> 22) % bot->numshards;
}

dpp::task<dpp::http_request_completion_t> LavalinkConnector::requestGet(
    const std::string& endpoint)
{
    auto url = hostUrl + endpoint;
    co_return co_await bot->co_request(url, dpp::m_get, "", "", {authHeader});
}

std::string LavalinkConnector::sessionEndpoint()
{
    return "/" + apiVersion + "/sessions/" + ws->getSessionId();
}

PlayerController* LavalinkConnector::getPlayerController(
    const dpp::snowflake& guildId)
{
    std::lock_guard<std::mutex> lock(connectedPlayersMutex);
    if (not playerControllers.contains(guildId))
    {
        playerControllers[guildId] =
            std::make_unique<PlayerController>(ws, this, bot, guildId);
    }
    return playerControllers[guildId].get();
}
}  // namespace lavalink
