

#include "lavalink/PlayerController.hpp"

#include <dpp/dpp.h>

#include "helpers.hpp"
#include "lavalink/LavalinkConnector.hpp"
#include "lavalink/types.hpp"

namespace lavalink
{
PlayerController::PlayerController(LavalinkWebsocket* ws_,
                                   LavalinkConnector* owner_,
                                   dpp::cluster* bot_, dpp::snowflake guildId_)
    : ws(ws_), owner(owner_), bot(bot_), guildId(guildId_)
{
}

void PlayerController::onPlayerUpdate(const PlayerUpdateEvent&)
{
    
    bot->log(dpp::ll_debug, "Player update event received.");
}

void PlayerController::onTrackStart(const TrackStartEvent&)
{
    playing = true;
    bot->log(dpp::ll_debug, "Track start event received.");
}

void PlayerController::onTrackEnd(const TrackEndEvent&)
{
    playing = false;
    bot->log(dpp::ll_debug, "Track end event received.");
}

void PlayerController::onTrackException(const TrackExceptionEvent&)
{
    playing = false;
    bot->log(dpp::ll_debug, "Track exception event received.");
}

void PlayerController::onTrackStuck(const TrackStuckEvent&)
{
    playing = false;
    bot->log(dpp::ll_debug, "Track stuck event received.");
}

void PlayerController::onWebSocketClosed(const WebSocketClosedEvent&)
{
    connected = false;
    playing = false;
    bot->log(dpp::ll_debug, "Websocket closed event received.");
}

void PlayerController::onVoiceServerUpdate(
    const dpp::voice_server_update_t& event)
{
    if (ignoreVoiceServerUpdate)
    {
        ignoreVoiceServerUpdate = false;
        return;
    }
    bot->log(dpp::ll_debug, "Voice server update event received.");
    voiceState.token = event.token;
    voiceState.endpoint = event.endpoint;

    nlohmann::json updatePlayerRequest = {{"voice", voiceState}};
    sendVoiceUpdate(updatePlayerRequest);
}

bool PlayerController::isConnected() const
{
    return connected;
}

dpp::task<bool> PlayerController::connect(const dpp::snowflake& channelId)
{
    (void)ws;
    if (connected)
    {
        co_return true;
    }

    auto shard =
        bot->get_shard(helpers::shardIdForGuild(guildId, bot->numshards));

    auto voiceServerUpdateAsync =
        helpers::awaitWhen(bot->on_voice_server_update,
                           [this](const dpp::voice_server_update_t& event)
                           { return event.guild_id == this->guildId; });
    auto voiceStateUpdateAsync = helpers::awaitWhen(
        bot->on_voice_state_update,
        [this](const dpp::voice_state_update_t& event)
        {
            return (event.state.guild_id == this->guildId) and
                   (event.state.user_id == this->bot->me.id);
        });

    nlohmann::json voiceconnRequest = {{"op", 4},
                                       {"d",
                                        {{"guild_id", guildId.str()},
                                         {"channel_id", channelId.str()},
                                         {"self_mute", false},
                                         {"self_deaf", false}}}};

    shard->queue_message(voiceconnRequest.dump());
    auto voiceStateUpdate = co_await voiceStateUpdateAsync;
    auto voiceServerUpdate = co_await voiceServerUpdateAsync;

    voiceState = {voiceServerUpdate.token, voiceServerUpdate.endpoint,
                  voiceStateUpdate.state.session_id};

    nlohmann::json updatePlayerRequest = {{"voice", voiceState}};
    ignoreVoiceServerUpdate = true;
    auto succesfull = co_await co_sendVoiceUpdate(updatePlayerRequest);

    if (succesfull)
    {
        connected = true;
        this->voiceChannelId = channelId;
        co_return true;
    }
    co_return false;
}

dpp::task<bool> PlayerController::co_sendVoiceUpdate(nlohmann::json& d,
                                                     bool noReplace)
{
    auto url = owner->hostUrl + owner->sessionEndpoint() + "/players/" +
               guildId.str() + "?noReplace=" + (noReplace ? "true" : "false");
    bot->log(dpp::ll_info, "Sending voice update request to " + url);
    bot->log(dpp::ll_debug, "Request body: " + d.dump());
    auto result = co_await bot->co_request(
        url, dpp::m_patch, d.dump(), "application/json", {owner->authHeader});

    bot->log(dpp::ll_debug, "Response: " + result.body);
    if (result.status != 200)
    {
        bot->log(dpp::ll_info, "Voice update request failed.");
        co_return false;
    }
    bot->log(dpp::ll_info, "Voice update request successful.");

    co_return true;
}

void PlayerController::sendVoiceUpdate(nlohmann::json& d, bool noReplace)
{
    auto url = owner->hostUrl + owner->sessionEndpoint() + "/players/" +
               guildId.str() + "?noReplace=" + (noReplace ? "true" : "false");
    bot->log(dpp::ll_info, "Sending voice update request to " + url);
    bot->log(dpp::ll_info, "Request body: " + d.dump());
    bot->request(url, dpp::m_patch,
                 [](const dpp::http_request_completion_t&) {}, d.dump(),
                 "application/json", {owner->authHeader});
}

dpp::snowflake PlayerController::getVoiceChannelId() const
{
    return voiceChannelId;
}

dpp::task<bool> PlayerController::disconnect()
{
    if (!connected)
    {
        co_return true;
    }

    nlohmann::json voiceconnRequest = {{"op", 4},
                                       {"d",
                                        {{"guild_id", guildId.str()},
                                         {"channel_id", nullptr},
                                         {"self_mute", false},
                                         {"self_deaf", false}}}};

    auto shard =
        bot->get_shard(helpers::shardIdForGuild(guildId, bot->numshards));
    shard->queue_message(voiceconnRequest.dump());

    sendVoiceDelete();
    voiceState = {};
    connected = false;
    voiceChannelId = 0;

    co_return true;
}

void PlayerController::sendVoiceDelete()
{
    auto url =
        owner->hostUrl + owner->sessionEndpoint() + "/players/" + guildId.str();
    bot->log(dpp::ll_info, "Sending voice delete request to " + url);
    bot->request(url, dpp::m_delete,
                 [](const dpp::http_request_completion_t&) {}, "",
                 "application/json", {owner->authHeader});
}

dpp::task<bool> PlayerController::playFromLink(const std::string& link)
{
    if (!connected)
    {
        co_return false;
    }

    auto loadRequestUrl = owner->hostUrl + '/' + owner->apiVersion +
                          "/loadtracks?identifier=" + link;

    auto result = co_await bot->co_request(loadRequestUrl, dpp::m_get, "", "",
                                           {owner->authHeader});

    bot->log(dpp::ll_info, "Response: " + result.body);
    if (result.status != 200)
    {
        bot->log(dpp::ll_info, "Failed to load track from link.");
        co_return false;
    }

    nlohmann::json loadResponse = nlohmann::json::parse(result.body);
    UpdatePlayerTrack trackToPlay{.encoded = loadResponse["data"]["encoded"]};
    nlohmann::json playRequest = {{"track", trackToPlay}};

    auto succesfull = co_await co_sendVoiceUpdate(playRequest, false);
    if (not succesfull)
    {
        co_return false;
    }

    co_return true;
}

bool PlayerController::isPlaying() const
{
    return playing;
}

dpp::task<bool> PlayerController::stop()
{
    if (!connected)
    {
        co_return false;
    }

    nlohmann::json stopRequest;
    stopRequest["track"]["encoded"] = nullptr;

    auto succesfull = co_await co_sendVoiceUpdate(stopRequest, false);
    if (not succesfull)
    {
        co_return false;
    }

    co_return true;
}

bool PlayerController::isPaused() const
{
    return paused;
}

dpp::task<bool> PlayerController::pause()
{
    if (!connected)
    {
        co_return false;
    }

    if (paused)
    {
        co_return true;
    }

    nlohmann::json pauseRequest;
    pauseRequest["paused"] = true;

    auto succesfull = co_await co_sendVoiceUpdate(pauseRequest, false);
    if (not succesfull)
    {
        co_return false;
    }

    paused = true;
    co_return true;
}

dpp::task<bool> PlayerController::resume()
{
    if (!connected)
    {
        co_return false;
    }

    if (not paused)
    {
        co_return true;
    }

    nlohmann::json resumeRequest;
    resumeRequest["paused"] = false;

    auto succesfull = co_await co_sendVoiceUpdate(resumeRequest, false);
    if (not succesfull)
    {
        co_return false;
    }

    paused = false;
    co_return true;
}
}  // namespace lavalink
