#include "lavalink/events.hpp"

namespace lavalink
{
const DispatchedEvent& DispatchedEvent::cancel_event() const
{
    cancelled = true;
    return *this;
}

DispatchedEvent DispatchedEvent::cancel_event()
{
    cancelled = true;
    return *this;
}

bool DispatchedEvent::is_cancelled() const
{
    return cancelled;
}
}  // namespace lavalink
