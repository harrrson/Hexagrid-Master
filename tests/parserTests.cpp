#include <gtest/gtest.h>
#define HEXA_TESTING
#include "commands/diceParser.hpp"
namespace diceParser
{
constexpr int MIN_DICE_COUNT = 1;
constexpr int MAX_DICE_COUNT = 1000;
constexpr int MIN_DICE_SIZE = 2;
constexpr int MAX_DICE_SIZE = 1000;
constexpr int MAX_DICE_MOD = 1000;
class DiceParser : public testing::Test
{
protected:
    void SetUp() override
    {
        diceParser::setLimits(MIN_DICE_COUNT, MAX_DICE_COUNT, MIN_DICE_SIZE, MAX_DICE_SIZE, MAX_DICE_MOD);
    }
};

TEST_F(DiceParser, errorOnDiceCountTooLow_diceCountOutOfRange)
{
    std::string dice{"0D10"};
    auto result = parseDice(dice);
    EXPECT_EQ(result.errorCause, ErrorCause::DICE_COUNT_OUT_OF_RANGE);
}

TEST_F(DiceParser, errorOnDiceCountTooHigh_diceCountOutOfRange)
{
    //Assuming MAX_DICE_COUNT is less than INT_MAX
    //Should be, because we do not have space in message for that much dices
    std::string dice{std::to_string(MAX_DICE_COUNT+1)+"D10"};
    auto result = parseDice(dice);
    EXPECT_EQ(result.errorCause, ErrorCause::DICE_COUNT_OUT_OF_RANGE);
}

TEST_F(DiceParser, parseD5)
{
    std::string dice{"D5"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 1);
    EXPECT_EQ(result.dice.size, 5);
    EXPECT_EQ(result.dice.modifierType, '+');
    EXPECT_EQ(result.dice.modifier, 0);
}

TEST_F(DiceParser, parse14D27)
{
    std::string dice{"14D27"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 14);
    EXPECT_EQ(result.dice.size, 27);
    EXPECT_EQ(result.dice.modifierType, '+');
    EXPECT_EQ(result.dice.modifier, 0);
}

TEST_F(DiceParser, ErrorOnDiceSizeTooLow_diceSizeOutOfRange)
{
    std::string dice{"D" + std::to_string(MIN_DICE_SIZE-1)};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::DICE_SIZE_OUT_OF_RANGE);
}

TEST_F(DiceParser, ErrorOnDiceSizeTooHigh_diceSizeOutOfRange)
{
    std::string dice{"D" + std::to_string(MAX_DICE_SIZE+1)};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::DICE_SIZE_OUT_OF_RANGE);
}

TEST_F(DiceParser, ShowFirstEncounteredError)
{
    std::string dice{"0d0"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::DICE_COUNT_OUT_OF_RANGE);
}

TEST_F(DiceParser, parseD10Plus123)
{
    std::string dice{"D10+123"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 1);
    EXPECT_EQ(result.dice.size, 10);
    EXPECT_EQ(result.dice.modifierType, '+');
    EXPECT_EQ(result.dice.modifier, 123);
}

TEST_F(DiceParser, parseD14Minus53)
{
    std::string dice{"D14-53"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 1);
    EXPECT_EQ(result.dice.size, 14);
    EXPECT_EQ(result.dice.modifierType, '-');
    EXPECT_EQ(result.dice.modifier, 53);
}

TEST_F(DiceParser, parseD15Mul54)
{
    std::string dice{"D15*54"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 1);
    EXPECT_EQ(result.dice.size, 15);
    EXPECT_EQ(result.dice.modifierType, '*');
    EXPECT_EQ(result.dice.modifier, 54);
}

TEST_F(DiceParser, parseD16Div55)
{
    std::string dice{"D16/55"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::NO_ERROR);
    EXPECT_EQ(result.dice.count, 1);
    EXPECT_EQ(result.dice.size, 16);
    EXPECT_EQ(result.dice.modifierType, '/');
    EXPECT_EQ(result.dice.modifier, 55);
}

TEST_F(DiceParser, ErrorOnModifierTooHigh_diceModOutOfRange)
{
    std::string dice{"D16+"+std::to_string(MAX_DICE_MOD+1)};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::DICE_MOD_OUT_OF_RANGE);
}

TEST_F(DiceParser, ErrorOnDivisionBy0_divBy0)
{
    std::string dice{"D16/0"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::DIV_BY_0);
}

TEST_F(DiceParser, ErrorOn2OrMoreD_ImproperCommandFormat)
{
    std::string dice{"D16d25"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::IMPROPER_COMMAND_FORMAT);
}

TEST_F(DiceParser, ErrorWhenNotUsedCharUsed_ImproperCommandFormat)
{
    std::string dice{"D16f"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::IMPROPER_COMMAND_FORMAT);
}

TEST_F(DiceParser, ErrorWhenNoNumberGiven_ImproperCommandFormat)
{
    std::string dice{"D"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::IMPROPER_COMMAND_FORMAT);
}

TEST_F(DiceParser, ErrorWhenOnlySymbolsWithoutNumberSent_ImproperCommandFormat)
{
    std::string dice{"D+"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::IMPROPER_COMMAND_FORMAT);
}

TEST_F(DiceParser, CommandWithoutDOnlyDIceSizeAdModifier_ImproperCommandFormat)
{
    std::string dice{"50+5"};
    auto result = parseDice(dice);

    EXPECT_EQ(result.errorCause, ErrorCause::IMPROPER_COMMAND_FORMAT);
}
}